<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        CONST CURSANDO = 1;//Estado de inscripcion
        CONST RECURSANDO = 3;//Estado de inscripcion
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
                    die();
                }
                if($this->router->fetch_method()!='seleccionarFacultad' && empty($this->user->facultad)){
                    header("Location:".base_url('panel/seleccionarFacultad?redirect='.$_SERVER['REQUEST_URI']));
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operaci贸n','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view' => 'main'))
        {
            if(empty($_SESSION['user'])){
                header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            }            
            else{
                if(!empty($param->output)){
                    $param->view = empty($param->view)?'panel':$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                parent::loadView($param);            
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        function seleccionarFacultad($x = '',$nombre = '',$logo = ''){
            if(!empty($_GET['redirect']) && empty($_SESSION['redirect'])){
                $_SESSION['redirect'] = $_GET['redirect'];
            }
            if(!empty($x) && is_numeric($x)){
                $this->user->setVariable('facultad',$x);
                $this->user->setVariable('facultadName',  urldecode($nombre));
                $this->user->setVariable('facultadLogo',  urldecode($logo));
                $this->user->reglog('concedido','Selección de facultad');
                if(!empty($_GET['redirect'])){
                    redirect($_GET['redirect']);
                    unset($_SESSION['redirect']);
                }
                redirect('panel');
            }
            else{
                $crud = new ajax_grocery_CRUD();
                $crud->set_table('facultades');
                $crud->set_theme('bootstrap2');
                $crud->set_subject('Seleccione una Dependencia');
                $this->db->group_by('facultades.id');
                $facultades = $this->user->getAccess('facultades.*');
                foreach($facultades->result() as $p){
                    $crud->or_where('id',$p->id);
                }
                if($facultades->num_rows()==0){
                    $crud->where('id','-1');
                }                
                if($facultades->num_rows()==1){
                    if(empty($_GET['redirect'])){
                        redirect('panel/seleccionarFacultad/'.$facultades->row()->id.'/'.$facultades->row()->nombre.'/'.$facultades->row()->foto);
                    }else{
                        redirect('panel/seleccionarFacultad/'.$facultades->row()->id.'/'.$facultades->row()->nombre.'/'.$facultades->row()->foto.'?redirect='.$_GET['redirect']);
                    }
                }
                $crud->unset_add()
                        ->unset_edit()
                        ->unset_delete()
                        ->unset_print()
                        ->unset_export()
                        ->unset_read();
                $crud->set_field_upload('foto','img/fotos_facultades');
                $crud->columns('foto','nombre');
                $crud->callback_column('nombre',function($val,$row){
                    $facultad = get_instance()->db->get_where('facultades',array('id'=>$row->id))->row();
                    if(!empty($_SESSION['redirect'])){
                        return '<a href="'.base_url('panel/seleccionarFacultad/'.$row->id.'/'.$row->nombre).'/'.$facultad->foto.'?redirect='.$_SESSION['redirect'].'">'.$val.'</a>';
                    }
                    return '<a href="'.base_url('panel/seleccionarFacultad/'.$row->id.'/'.$row->nombre).'/'.$facultad->foto.'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccion de Dependencia';
                $this->loadView($crud);
            }
        }       

        function seleccionarCarrera($x = '',$nombre = ''){
            if(!empty($x) && is_numeric($x)){
                $this->user->setVariable('programacion_carreras',$x);                
                $this->user->setVariable('programacion_carrerasNombre',urldecode($nombre));                
                $this->user->reglog('concedido','Selección de programacion de carrera');
                redirect('alumnos/mis_carreras');
            }
            else{
                $crud = new ajax_grocery_CRUD();
                $crud->set_table('programacion_carreras');
                $crud->set_theme('bootstrap2');
                $crud->set_subject('Seleccione una Carrera');
                $this->db->select('programacion_carreras_id as id, programacion_nombre as nombre');
                $this->db->join('estudiantes_carreras','estudiantes_carreras.estudiantes_id = estudiantes.id');
                $this->db->join('programacion_carreras','programacion_carreras.id = estudiantes_carreras.programacion_carreras_id');
                $this->db->group_by('programacion_carreras_id');
                $facultades = $this->db->get_where('estudiantes',array('user_id'=>$this->user->id));
                
                foreach($facultades->result() as $p){
                    $crud->or_where('id',$p->id);
                }
                if($facultades->num_rows()==0){
                    $crud->where('id','-1');
                }
                if($facultades->num_rows()==1){
                    redirect('panel/seleccionarCarrera/'.$facultades->row()->id.'/'.$facultades->row()->nombre);
                }
                $crud->unset_add()
                        ->unset_edit()
                        ->unset_delete()
                        ->unset_print()
                        ->unset_export()
                        ->unset_read();                
                $crud->columns('programacion_nombre');
                $crud->callback_column('programacion_nombre',function($val,$row){
                    return '<a href="'.base_url('panel/seleccionarCarrera/'.$row->id.'/'.$row->programacion_nombre).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $crud->title = 'Seleccion de Carrera';
                $this->loadView($crud);
            }
        }           
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
