<?php 

class Planacademico_model extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    function alreadyhavecarrer($estudiante){
        return $this->db->get_where('estudiantes_carreras',array('estudiante'=>$estudiante))->num_rows()>0?TRUE:FALSE;
    }
    
    function getDetallesMateriasPlan($id){
        $this->db->select('programacion_carreras.programacion_nombre as programacion_carrera, plan_estudio.fecha_creacion, plan_estudio.fecha_inicio_vigencia, plan_estudio.fecha_fin_vigencia, plan_estudio.codigo as codigo_plan, facultades.nombre as facultad');        
        $this->db->join('facultades','facultades.id = plan_estudio.facultades_id');
        $this->db->join('programacion_carreras','programacion_carreras.id = plan_estudio.programacion_carreras_id');
        return $this->db->get_where('plan_estudio',array('plan_estudio.id'=>$id))->row();
    }
    
    function getDetallesMaterias($id){
        $this->db->select('programacion_carreras.programacion_nombre as programacion_carrera, plan_estudio.codigo, facultades.nombre as facultad, materias.materia_nombre as materia, materias_plan.plan, plan_estudio.programacion_carreras_id as programacion, facultades.id as facultad_id, materias_plan.materia as materia_id');        
        $this->db->join('materias','materias.id = materias_plan.materia');
        $this->db->join('plan_estudio','plan_estudio.id = materias_plan.plan');
        $this->db->join('facultades','facultades.id = plan_estudio.facultades_id');
        $this->db->join('programacion_carreras','programacion_carreras.id = plan_estudio.programacion_carreras_id');
        return $this->db->get_where('materias_plan',array('materias_plan.id'=>$id))->row();
    }
    
    function getDetallesInscripcionMaterias($id){       
        $this->db->select('matriculas.id,user.nombre, matriculas.anho_lectivo, user.apellido_paterno, programacion_carreras.programacion_nombre, plan_estudio.plan_nombre, plan_estudio.id as plan_id, estado_matricula.estado_matricula_nombre');
        $this->db->join('estudiantes','estudiantes.id = matriculas.estudiantes_id');
        $this->db->join('user','user.id = estudiantes.user_id');
        $this->db->join('programacion_carreras','programacion_carreras.id = matriculas.programacion_carreras_id');
        $this->db->join('plan_estudio','plan_estudio.id = matriculas.plan_estudio_id');
        $this->db->join('estado_matricula','estado_matricula.id = matriculas.estado_matricula_id');        
        $this->db->where('matriculas.id',$id);
        $re = $this->db->get_where('matriculas');        
        if($re->num_rows()>0){            
            return $re->row();
        }       
        throw new Exception('<b>ERROR:404</b> No hemos encontrado al estudiante seleccionado','404');       
    }
    
    function getInscripcionListMaterias($plan_id = '',$anho_lectivo = ''){
        $this->db->select('materias.materia_nombre, programacion_materias_plan.id, programacion_materias_plan.anho_lectivo, cursos.curso_nombre, secciones.seccion_nombre')
                 ->join('materias_plan','materias_plan.id = programacion_materias_plan.materias_plan_id','INNER')
                 ->join('materias','materias.id = materias_plan.materias_id','INNER')
                 ->join('cursos','cursos.id = materias_plan.cursos_id','INNER')
                 ->join('secciones','secciones.id = programacion_materias_plan.secciones_id','INNER');
        
        if(!empty($plan_id)){
            $this->db->where('materias_plan.plan_estudio_id',$plan_id);
        }
        if(!empty($anho_lectivo)){
            $this->db->where('programacion_materias_plan.anho_lectivo',$anho_lectivo);
        }
        if(empty($plan_id) && empty($anho_lectivo)){
            $this->db->limit(100);
        }
        $this->db->order_by('materia_nombre','ASC');
        return $this->db->get('programacion_materias_plan');
    }
    
     function getInscripcionListAlumnos($programacion_materias_plan_id = '',$anho_lectivo = '',$horario = ''){
        $this->db->select('inscripcion_materias.id,inscripcion_materias.programacion_materias_plan_id, user.nombre, user.apellido_paterno, user.cedula, user.apellido_materno, estudiantes.id as estudiantes_id');
        $this->db->join('matriculas','matriculas.id = inscripcion_materias.matriculas_id');
        $this->db->join('estudiantes','estudiantes.id = matriculas.estudiantes_id');
        $this->db->join('estudiantes_carreras','estudiantes_carreras.estudiantes_id = estudiantes.id');        
        $this->db->join('user','user.id = estudiantes.user_id');
        if(!empty($horario)){
            //$this->db->join('inscripcion_mesa_final','inscripcion_mesa_final.estudiantes_carreras_id = estudiantes_carreras.id','LEFT');
            //Añadimos los datos de evaluaciones parciales
            /*$this->db->select('inscripcion_materias.id, user.nombre, user.apellido_paterno, user.cedula, user.apellido_materno');
            $this->db->join('evaluaciones_parciales_detalles','evaluaciones_parciales_detalles.inscripcion_materias_id = inscripcion_materias.id');
            $this->db->join('evaluaciones_parciales','evaluaciones_parciales.id = evaluaciones_parciales_detalles.evaluaciones_parciales_id');*/
        }
        $this->db->where('inscripcion_materias.programacion_materias_plan_id',$programacion_materias_plan_id);
        $this->db->where('matriculas.anho_lectivo',$anho_lectivo);
        $this->db->order_by('apellido_paterno','ASC');
        $this->db->order_by('nombre','ASC');
        $this->db->group_by('inscripcion_materias.id');
        $inscripcion_materias = $this->db->get('inscripcion_materias');
        
        if(!empty($horario)){
            foreach($inscripcion_materias->result() as $n=>$i){
                $evaluacion = $this->db->query('
                SELECT 
                evaluaciones_parciales_id, 
                inscripcion_materias_id, 
                sum(evd.puntos_correctos) AS puntoscorrectos,
                con_recuperatorio.totalpuntos as con_recuperatorio_total_puntos,
                sin_recuperatorio.totalpuntos as sin_recuperatorio_total_puntos,
                sin_recuperatorio.porc_proceso as porc_proceso,
                sin_recuperatorio.escala_id as escala_id,
                sin_recuperatorio.porc_final as porc_final,
                
                (
                    SELECT CASE
                        WHEN con_recuperatorio.totalpuntos IS NOT NULL THEN (sum(evd.puntos_correctos) * sin_recuperatorio.porc_proceso / con_recuperatorio.totalpuntos)

                        WHEN con_recuperatorio.totalpuntos IS NULL THEN (sum(evd.puntos_correctos) * sin_recuperatorio.porc_proceso / sin_recuperatorio.totalpuntos)

                    END AS p_proceso
                ) as p_proceso

                FROM evaluaciones_parciales_detalles evd

                INNER JOIN evaluaciones_parciales as ev ON ev.id = evd.evaluaciones_parciales_id
                INNER JOIN materias_plan as mp ON ev.programacion_materias_plan_id = mp.id
                INNER JOIN parametro_evaluacion as pe ON mp.parametro_evaluacion_id = pe.id
                LEFT JOIN (
                        SELECT programacion_materias_plan_id, sum(total_puntos) AS totalpuntos, porc_proceso, escala_id, recup_reemp, porc_final
                    FROM evaluaciones_parciales
                    INNER JOIN materias_plan ON evaluaciones_parciales.programacion_materias_plan_id = materias_plan.id
                    INNER JOIN parametro_evaluacion ON materias_plan.parametro_evaluacion_id = parametro_evaluacion.id
                    WHERE tipos_evaluacion_id != 4
                    GROUP BY programacion_materias_plan_id
                ) AS sin_recuperatorio ON sin_recuperatorio.programacion_materias_plan_id = ev.programacion_materias_plan_id

                LEFT JOIN (
                        SELECT programacion_materias_plan_id, sum(total_puntos) AS totalpuntos, porc_proceso, escala_id, recup_reemp
                    FROM evaluaciones_parciales
                    INNER JOIN materias_plan ON evaluaciones_parciales.programacion_materias_plan_id = materias_plan.id
                    INNER JOIN parametro_evaluacion ON materias_plan.parametro_evaluacion_id = parametro_evaluacion.id
                    WHERE tipos_evaluacion_id = 4
                    GROUP BY programacion_materias_plan_id
                ) AS con_recuperatorio ON con_recuperatorio.programacion_materias_plan_id = ev.programacion_materias_plan_id


                WHERE evd.anulado = FALSE AND evd.inscripcion_materias_id = '.$i->id.'

                GROUP BY inscripcion_materias_id');
                if($evaluacion->num_rows()>0){
                    $inscripcion_materias->row($n)->p_proceso = $evaluacion->row()->porc_proceso;                
                    $inscripcion_materias->row($n)->porc_final = $evaluacion->row()->porc_final;
                    $escalas = array();
                    foreach($this->db->get_where('escala_detalle',array('escala_id'=>$evaluacion->row()->escala_id))->result() as $e){
                        $escalas[] = $e;
                    }
                    $inscripcion_materias->row($n)->escalas = $escalas;
                }else{
                    $inscripcion_materias->row($n)->p_proceso = 0;                
                    $inscripcion_materias->row($n)->porc_final = 0;
                    $inscripcion_materias->row($n)->escalas = array();
                }
            }
        }
        
        return $inscripcion_materias;
    }
    
    function getProgramacionMateriasPlan($plan_estudio,$anho_lectivo){
        $this->db->select('materias.materia_nombre, programacion_materias_plan.id, cursos.curso_nombre, secciones.seccion_nombre');
        $this->db->join('materias_plan','materias_plan.id = programacion_materias_plan.materias_plan_id')
                 ->join('materias','materias.id = materias_plan.materias_id')
                 ->join('cursos','materias_plan.cursos_id = cursos.id')
                 ->join('secciones','secciones.id = programacion_materias_plan.secciones_id','left');
        $this->db->where('plan_estudio_id',$plan_estudio)
                 ->where('anho_lectivo',$anho_lectivo);
        return $this->db->get('programacion_materias_plan');
    }
    
    function getProgramacionMateriasPlanFromAsistencia($materias_plan_id = '',$anho_lectivo = ''){
        
        $this->db->select('materias.materia_nombre, programacion_materias_plan.id, cursos.curso_nombre, secciones.seccion_nombre');
        $this->db->join('materias_plan','materias_plan.id = programacion_materias_plan.materias_plan_id')
                 ->join('secciones','programacion_materias_plan.secciones_id = secciones.id')
                 ->join('materias','materias.id = materias_plan.materias_id')
                 ->join('cursos','materias_plan.cursos_id = cursos.id');
        $this->db->where('materias_plan_id',$materias_plan_id)
                 ->where('anho_lectivo',$anho_lectivo);
        return $this->db->get('programacion_materias_plan');
    }
    
    function getMateriasFromAlumn($matriculas_id,$estado){
        $this->db->where('inscripcion_materias.estado_inscripcion',$estado);
        $this->db->where('inscripcion_materias.matriculas_id',$matriculas_id);        
        $l = array();
        foreach($this->db->get('inscripcion_materias')->result() as $i){
            $l[] = $i->programacion_materias_plan_id;
        } 
        return $l;
    }
    
    function getDocentes(){
        $this->db->select('docentes.id, user.nombre, user.apellido_paterno');
        $this->db->join('user','user.id = docentes.user_id');
        return $this->db->get('docentes');
    }
}
