<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>About Us I</h1>
                <h6>About Fable</h6>
            </div>
        </div>

    </div>
</div>

<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main ">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Our Session Plan</h2>
                <h6>We offer a quality children care and friendly atmosphere</h6>
                <div></div>
            </div>	

            <!-- Pricing plans -->
            <div class="template-component-pricing-plan template-component-pricing-plan-style-2">
                <ul class="template-layout-50x50 template-clear-fix">
                    <li class="template-layout-column-left template-component-pricing-plan-background-1">
                        <div>
                            <div class="template-component-pricing-plan-price">
                                <span>$15</span>
                                <span>/ day</span>
                            </div>
                            <h5 class="template-component-pricing-plan-header">
                                Morning Session
                            </h5>
                            <div class="template-component-pricing-plan-description">
                                Movum elementum pulvinar detos morbi a dosis retrum gravida.
                            </div>
                            <div class="template-component-pricing-plan-feature">
                                <div class="template-component-list template-component-list-style-1">
                                    <ul>
                                        <li>8 am - 12 Noon Care</li>
                                        <li>3 Meals a Day</li>
                                        <li>Science Classes</li>
                                        <li>2 Educators</li>	
                                    </ul>
                                </div>
                            </div>
                            <div class="template-component-pricing-plan-button">
                                <a href="<?= site_url('p/cicles-detail') ?>" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                            </div>
                        </div>
                    </li>
                    <li class="template-layout-column-right template-component-pricing-plan-background-2">
                        <div>
                            <div class="template-component-pricing-plan-price">
                                <span>$25</span>
                                <span>/ day</span>
                            </div>
                            <h5 class="template-component-pricing-plan-header">
                                Full Day Session
                            </h5>
                            <div class="template-component-pricing-plan-description">
                                Movum elementum pulvinar detos morbi a dosis retrum gravida.
                            </div>
                            <div class="template-component-pricing-plan-feature">
                                <div class="template-component-list template-component-list-style-1">
                                    <ul>
                                        <li>8 am - 12 Noon Care</li>
                                        <li>3 Meals a Day</li>
                                        <li>Science Classes</li>
                                        <li>3 Educators</li>	
                                    </ul>
                                </div>
                            </div>
                            <div class="template-component-pricing-plan-button">
                                <a href="<?= site_url('p/cicles-detail') ?>" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                            </div>
                        </div>			
                    </li>

                    <li class="template-layout-column-left template-component-pricing-plan-background-2">
                        <div>
                            <div class="template-component-pricing-plan-price">
                                <span>$25</span>
                                <span>/ day</span>
                            </div>
                            <h5 class="template-component-pricing-plan-header">
                                Full Day Session
                            </h5>
                            <div class="template-component-pricing-plan-description">
                                Movum elementum pulvinar detos morbi a dosis retrum gravida.
                            </div>
                            <div class="template-component-pricing-plan-feature">
                                <div class="template-component-list template-component-list-style-1">
                                    <ul>
                                        <li>8 am - 12 Noon Care</li>
                                        <li>3 Meals a Day</li>
                                        <li>Science Classes</li>
                                        <li>3 Educators</li>	
                                    </ul>
                                </div>
                            </div>
                            <div class="template-component-pricing-plan-button">
                                <a href="<?= site_url('p/cicles-detail') ?>" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                            </div>
                        </div>			
                    </li>

                    <li class="template-layout-column-right template-component-pricing-plan-background-2">
                        <div>
                            <div class="template-component-pricing-plan-price">
                                <span>$25</span>
                                <span>/ day</span>
                            </div>
                            <h5 class="template-component-pricing-plan-header">
                                Full Day Session
                            </h5>
                            <div class="template-component-pricing-plan-description">
                                Movum elementum pulvinar detos morbi a dosis retrum gravida.
                            </div>
                            <div class="template-component-pricing-plan-feature">
                                <div class="template-component-list template-component-list-style-1">
                                    <ul>
                                        <li>8 am - 12 Noon Care</li>
                                        <li>3 Meals a Day</li>
                                        <li>Science Classes</li>
                                        <li>3 Educators</li>	
                                    </ul>
                                </div>
                            </div>
                            <div class="template-component-pricing-plan-button">
                                <a href="<?= site_url('p/cicles-detail') ?>" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                            </div>
                        </div>			
                    </li>
                </ul>
            </div>

        </div>

    </div>



    <!-- Section -->
    <div class="template-content-section">

        <!-- Main -->
        <div class="template-main">

            <!-- Tab -->
            <div class="template-component-tab">
                <ul>
                    <li>
                        <a href="#template-tab-1">Valentine&#039;s Day</a>
                        <span></span>
                    </li>
                    <li>
                        <a href="#template-tab-2">City ZOO Trip</a>
                        <span></span>
                    </li>
                    <li>
                        <a href="#template-tab-3">Mother&#039; Day</a>
                        <span></span>
                    </li>
                </ul>
                <div id="template-tab-1">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-33x33x33 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/5.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Classes in Writing</h6>
                                        <span>Shining Stars Class</span>
                                    </div>	
                                    <p><b>Classes in Writing</b> Shining Stars Class</p>
                                </div>
                            </li>
                            <li class="template-layout-column-center">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/6.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/6.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Classes in Painting</h6>
                                        <span>Shining Stars Class</span>
                                    </div>
                                    <p><b>Classes in Painting</b> Shining Stars Class</p>
                                </div>			
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/7.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/7.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Drawing and Painting Lessons</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                                </div>
                            </li>
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/1.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/1.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Outdoor Activity During Recess</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                                </div>			
                            </li>
                            <li class="template-layout-column-center">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/8.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/8.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Columbus Day Celebration</h6>
                                        <span>Bouncy Bears Class</span>
                                    </div>
                                    <p><b>Columbus Day Celebration</b> Bouncy Bears Class</p>
                                </div>
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/9.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/9.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Birthday in Kindergarten</h6>
                                        <span>Little Lambs Class</span>
                                    </div>
                                    <p><b>Birthday in Kindergarten</b> Little Lambs Class</p>
                                </div>			
                            </li>
                        </ul>
                    </div>	

                </div>
                <div id="template-tab-2">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-50x50 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/5.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Classes in Writing</h6>
                                        <span>Shining Stars Class</span>
                                    </div>	
                                    <p><b>Classes in Writing</b> Shining Stars Class</p>
                                </div>
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/6.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/6.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Classes in Painting</h6>
                                        <span>Shining Stars Class</span>
                                    </div>
                                    <p><b>Classes in Painting</b> Shining Stars Class</p>
                                </div>			
                            </li>
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/7.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/7.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Drawing and Painting Lessons</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                                </div>
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/1.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/1.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Outdoor Activity During Recess</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                                </div>			
                            </li>
                        </ul>
                    </div>	

                </div>
                <div id="template-tab-3">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-25x25x25x25 template-layout-margin-reset template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/5.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Classes in Writing</h6>
                                        <span>Shining Stars Class</span>
                                    </div>	
                                    <p><b>Classes in Writing</b> Shining Stars Class</p>
                                </div>
                            </li>
                            <li class="template-layout-column-center-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/6.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/6.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Classes in Painting</h6>
                                        <span>Shining Stars Class</span>
                                    </div>
                                    <p><b>Classes in Painting</b> Shining Stars Class</p>
                                </div>			
                            </li>
                            <li class="template-layout-column-center-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/7.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/7.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Drawing and Painting Lessons</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                                </div>
                            </li>
                            <li class="template-layout-column-right">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/1.jpg" data-fancybox-group="gallery-2">
                                        <img src="<?= base_url() ?>img/_sample/690x506/1.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Outdoor Activity During Recess</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                                </div>			
                            </li>
                        </ul>
                    </div>	

                </div>
            </div>

        </div>

    </div>


    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <h4>Our goal is to create a place that engages each child.</h4>

                    <!-- List -->
                    <div class="template-component-list template-component-list-style-3">
                        <ul>
                            <li>Comprehensive reporting on individual achievement</li>
                            <li>Educational field trips and school presentations</li>
                            <li>Individual attention in a small-class setting</li>
                            <li>Learning program with after-school care</li>
                            <li>Opportunities to carry out scientific investigations</li>
                            <li>Positive learning environment for your child</li>
                            <li>Delightful, friendly and welcoming place</li>									
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <!-- Accordion -->
                    <div class="template-component-accordion">
                        <h6><a href="#">Educational Field Trips and Presentations</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Reporting on Individual Achievement</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>										
                        <h6><a href="#">Writing and Reading Classes</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Scientific Investigation Opportunities</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>