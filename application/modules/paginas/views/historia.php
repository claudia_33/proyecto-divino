<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Història</h1>
                <h6>Més de 80 anys aprenent junts</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3>Els inicis <strong>de l'Escola Monalco</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            L’origen del Col·legi MONALCO és l’Acadèmia Cots que s’inaugurà a Igualada pels volts del 1930 en un  local situat en el núm. 30 del carrer d’Òdena.
                        </div>

                        <p class="template-margin-top-3">
                            L’any 1946, el fundador de la nostra escola, el Sr. Pere Vicente va pensar en fer el primer ensenyament amb els alumnes més petits. S’hagué de buscar un nou local que fou el situat al carrer de Sant Magí, 58; se li va posar el nom de Col·legi Verdaguer. 
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">1930-1946 <i></i></a>

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/10.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Play Time In Kindergarten</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Play Time In Kindergarten</b> Tenderhearts Class</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/11.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Drawing and Painting Lessons</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3>Unificació de l'escola <strong>concentració en un sol lloc</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            El nostre fundador va començar a construir i traslladar tot el complex escolar que dirigia, als terrenys de l’Avinguda Barcelona-Carrer Capellades.
                        </div>

                        <p class="template-margin-top-3">
                            L’escola fou construïda d’acord amb la idea que l’arquitectura escolar ha d’ésser com l’educació: comunitària, oberta i harmònica, i dotada, alhora, de les condicions adequades per a una formació integral de la persona.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">1975-1999 <i></i></a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left template-margin-bottom-reset">

                    <div class="template-align-center">

                        <h3>Escola consilidada com millor <strong> escola de la comarca</strong></h3>

                        <div class="template-component-divider template-component-divider-style-2"></div>

                        <div class="template-component-italic template-margin-top-3">
                            Nulla adiscipling elite forte, nodis est advance pulvinar maecenas est dolor, novum elite lacina.
                        </div>

                        <p class="template-margin-top-3">
                            Praesent arcu gravida a vehicula est node maecenas loareet maecenas morbi dosis luctus mode. Urna eget lacinia eleifend molibden dosis et gravida dosis sit amet terminal.
                        </p>

                        <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">1999-avui <i></i></a>

                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right template-margin-bottom-reset">

                    <!-- Gallery -->
                    <div class="template-component-gallery">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                    <a href="<?= base_url() ?>img/_sample/1050x770/5.jpg" data-fancybox-group="gallery-1">
                                        <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" alt="" />
                                        <span><span><span></span></span></span>
                                    </a>
                                    <div>
                                        <h6>Outdoor Activity During Recess</h6>
                                        <span>Tenderhearts Class</span>
                                    </div>
                                    <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-image template-background-image-4">

        <!-- Main -->
        <div class="template-main template-section-white">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-top template-component-feature-size-large">
                <ul class="template-layout-25x25x25x25 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-teddy-alt"></div>
                        <h5>Morbi Etos</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                    <li class="template-layout-column-center-left">
                        <div class="template-icon-feature template-icon-feature-name-heart-alt"></div>
                        <h5>Congue Gravida</h5>
                        <p>Elipsis magna a terminal nulla elementum morbi elite forte maecenas est magna etos interdum vitae est.</p>
                    </li>	
                    <li class="template-layout-column-center-right">
                        <div class="template-icon-feature template-icon-feature-name-graph-alt"></div>
                        <h5>Maecenas Node</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-globe-alt"></div>
                        <h5>Placerat Etos</h5>
                        <p>Praesent interdum est gravida vehicula est node maecenas loareet morbi a dosis luctus novum est praesent.</p>
                    </li>
                </ul>
            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-padding-top-reset">

        <!-- Main -->
        <div class="template-main">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>What&#039;s New</h2>
                <h6>Keep up to date with the latest news</h6>
                <div></div>
            </div>	

            <!-- Recent post -->
            <div class="template-component-recent-post template-component-recent-post-style-1">

                <!-- Layout 33x33x33 -->
                <ul class="template-layout-33x33x33 template-clear-fix">

                    <!-- Left column -->
                    <li class="template-layout-column-left">
                        <div class="template-component-recent-post-date">October 03, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/7.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">12</div>
                        </div>
                        <h5><a href="#">Drawing and Painting Lessons</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Events</a>,
                                <a href="#">Fun</a>
                            </li>
                        </ul>
                    </li>

                    <!-- Center column -->
                    <li class="template-layout-column-center">
                        <div class="template-component-recent-post-date">October 03, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/2.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">4</div>
                        </div>
                        <h5><a href="#">Fall Parents Meeting Day</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Dance</a>,
                                <a href="#">Education</a>
                            </li>
                        </ul>			
                    </li>

                    <!-- Right column -->
                    <li class="template-layout-column-right">
                        <div class="template-component-recent-post-date">September 20, 2014</div>
                        <div class="template-component-image template-preloader">
                            <a href="#">
                                <img src="<?= base_url() ?>img/_sample/690x414/9.jpg" alt=""/>
                                <span><span><span></span></span></span>
                            </a>
                            <div class="template-component-recent-post-comment-count">4</div>
                        </div>
                        <h5><a href="#">Birthday in Kindergarten</a></h5>
                        <p>Magna est consectetur interdum modest dictum. Curabitur est faucibus, malesuada esttincidunt etos et mauris, nunc a libero govum est cuprum.</p>
                        <ul class="template-component-recent-post-meta template-clear-fix">
                            <li class="template-icon-blog template-icon-blog-author"><a href="#">Anna Brown</a></li>
                            <li class="template-icon-blog template-icon-blog-category">
                                <a href="#">Games</a>,
                                <a href="#">General</a>
                            </li>
                        </ul>			
                    </li>

                </ul>

            </div>

        </div>

    </div>

</div>