<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Infantil</h1>
                <h6>De 3 a 6 anys</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-main">

        <!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <h2>Començem a formar al futur</h2>
            <h6>Descobrim nous talents, noves inquietuds...</h6>
            <div></div>
        </div>

        <!-- Layout 50x50 -->
        <div class="template-layout-50x50 template-clear-fix">

            <!-- Left column -->
            <div class="template-layout-column-left">

                <!-- Header -->
                <h4>Metodologia</h4>

                <p>Infantil treballa una sèrie d'obectius. És una edat en que tot el curiositats i ganes d'aprendre i nosaltres som allà per fer-ho aflorar.</p>

                <!-- List -->
                <div class="template-component-list template-component-list-style-1 template-margin-top-3">
                    <ul>
                        <li>Treballem el llenguatge i l'escriptura</li>
                        <li>Eduquem i corregim l'expressió</li>
                        <li>Donem molta importància als sentiments</li>
                        <li>Descobrim l'entorn</li>											
                    </ul>
                </div>

                <!-- Vertical grid -->
                <div class="template-component-vertical-grid template-margin-top-3">
                    <ul>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>P3:</div>
                            <div>3-4 anys</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>P4:</div>
                            <div>4-5 anys</div>
                        </li>
                        <li class="template-component-vertical-grid-line-1n">
                            <div>P5:</div>
                            <div>5-6 anys</div>
                        </li>
                        <li class="template-component-vertical-grid-line-2n">
                            <div>Horaris:</div>
                            <div>Matins, de 9 a 13h<br/>Tardes, de 15h a 17h</div>
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Right column -->
            <div class="template-layout-column-right">

                <!-- Nivo slider -->
                <div class="template-component-nivo-slider template-component-nivo-slider-style-1 template-preloader">
                    <div>
                        <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/10.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/5.jpg" alt=""/>
                        <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" data-thumb="<?= base_url() ?>img/_sample/690x506/11.jpg" alt=""/>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-1 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-app-alt"></div>
                        <h5>Objectius</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pin-alt"></div>
                        <h5>Activitats</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-piano-alt"></div>
                        <h5>Horaris</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>
                </ul>
            </div>

            <!-- Feature -->
            <div class="template-component-feature template-component-feature-style-2 template-component-feature-position-left template-component-feature-size-medium">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-icon-feature template-icon-feature-name-people"></div>
                        <h5>Professorat</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-icon-feature template-icon-feature-name-pencil"></div>
                        <h5>Horaris</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>	
                    <li class="template-layout-column-right">
                        <div class="template-icon-feature template-icon-feature-name-paintbrush"></div>
                        <h5>Imatges</h5>
                        <p>Pulvinar est metro ligula blandit maecenas retrum gravida cuprum.</p>			
                    </li>
                </ul>
            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-background-image template-background-image-4">
        <div class="template-main">

            <!-- Testimonials -->
            <div class="template-section-white">
                <div class="template-component-testimonial template-component-testimonial-style-2">
                    <ul class="template-layout-100">
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>Educar un nen no és fer-li aprendre alguna cosa que no sabia, sinó fer d'ell algú que no existia.</p>
                            <div></div>
                            <span>John Ruskin</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>Jo no ensenyo als meus alumnes, només els proporciono les condicions en què puguin aprendre.</p>
                            <div></div>
                            <span>Albert Einstein</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>El cervell no és un got per omplir, sinó un llum per encendre.</p>
                            <div></div>
                            <span>Plutarco</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>Un dels principals objectius de l'educación ha de ser ampliar les finestres per les que veiem el món.</p>
                            <div></div>
                            <span>Arnold Glasow</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>La clau no és ensenyar, és despertar.</p>
                            <div></div>
                            <span>Ernest Renan</span>
                        </li>
                        <li class="template-layout-column-left">
                            <i></i>
                            <p>L'art d'ensenyar és l'art d'ajudar a descobrir.</p>
                            <div></div>
                            <span>Mark Van Doren</span>
                        </li>
                    </ul>
                    <div class="template-pagination template-pagination-style-1"></div>
                </div>
            </div>			

        </div>
    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <!-- Accordion -->
                    <div class="template-component-accordion">
                        <h6><a href="#">Educational Field Trips and Presentations</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Reporting on Individual Achievement</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>										
                        <h6><a href="#">Writing and Reading Classes</a></h6>
                        <div>
                            <p>
                                Maecenas prion neque vuluptat sem in porttitil curabitur mattis, vitae elite forte, adiscipling elit. Novum elementum est dosis cuprum gravida.
                            </p>
                        </div>
                        <h6><a href="#">Scientific Investigation Opportunities</a></h6>
                        <div>
                            <p>
                                Phasellus consequat est eleifend, leo condimentum nec nllam ut lectus turpis. Nunc sharme nullam an suscipit bibendum sagittis.
                            </p>
                        </div>
                    </div>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <!-- Counters list -->
                    <div class="template-component-counter-list">
                        <ul class="template-layout-100 template-clear-fix">
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Art Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">165</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Writing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">125</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left">
                                <span class="template-component-counter-list-label">Drawing Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">52</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                            <li class="template-layout-column-left template-margin-bottom-reset">
                                <span class="template-component-counter-list-label">Yoga Classes</span>
                                <span class="template-component-counter-list-counter">
                                    <span class="template-component-counter-list-counter-value">75</span>
                                    <span class="template-component-counter-list-counter-character">Hours</span>
                                </span>
                                <span class="template-component-counter-list-timeline">
                                    <span></span>
                                </span>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-main template-padding-top-reset template-padding-bottom-5">

        <!-- Header and subheader -->
        <div class="template-component-header-subheader">
            <h2>Els mestres</h2>
            <h6>With education and experience in early childhood care</h6>
            <div></div>
        </div>

        <!-- Team -->
        <div class="template-component-team template-component-team-style-2">
            <ul class="template-layout-50x50 template-clear-fix">
                <li class="template-layout-column-left">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/11.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/11.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Linda Moore</h6>
                                    <span>Teacher</span>
                                </div>
                                <p><b>Linda Moore</b> Teacher</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">I hold a degree in Early Childhood Education and an advanced English language certificate. I have been working as a kindergarten teacher since 2002.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                </ul>
                            </div>					
                        </li>
                    </ul>			
                </li>
                <li class="template-layout-column-right">
                    <ul class="template-layout-50x50 template-clear-fix">
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/525x560/2.jpg" data-fancybox-group="team-2">
                                    <img src="<?= base_url() ?>img/_sample/525x560/2.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Claire Willmore</h6>
                                    <span>Teacher</span>
                                </div>
                                <p><b>Claire Willmore</b> Teacher</p>
                            </div>					
                        </li>
                        <li class="template-layout-column-right">
                            <div class="template-component-team-quote"></div>
                            <p class="template-component-team-description">I have just finished my studies in Early Childhood Education, and I am also the kid’s yoga teacher here at Fable. I enjoy cooking, swimming and bike riding in my free time.</p>
                            <div class="template-component-social-icon template-component-social-icon-style-1 template-clear-fix">
                                <ul>
                                    <li><a href="#" class="template-component-social-icon-mail"></a></li>
                                    <li><a href="#" class="template-component-social-icon-facebook"></a></li>
                                    <li><a href="#" class="template-component-social-icon-twitter"></a></li>
                                </ul>
                            </div>						
                        </li>
                    </ul>				
                </li>
            </ul>
        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">
        <div class="template-main">

            <!-- Call to action -->
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!-- Content -->
                <div class="template-component-call-to-action-content">

                    <!-- Left column -->
                    <div class="template-component-call-to-action-content-left">

                        <!-- Header -->
                        <h3>Vols contactar amb la classe d'infantil?</h3>

                    </div>

                    <!-- Right column -->
                    <div class="template-component-call-to-action-content-right">

                        <!-- Button -->
                        <a href="#" class="template-component-button template-component-button-style-1">Contacta aquí<i></i></a>

                    </div>									

                </div>

            </div>				

        </div>

    </div>

</div>