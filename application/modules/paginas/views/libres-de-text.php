<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Llibres de text</h1>
                <h6>Troba tot el material que necessites</h6>
            </div>
        </div>

    </div>
</div>

<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main ">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Primària</h2>
                <h6>Llibres i fitxes que cal tenir</h6>
                <div></div>
            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">
                    <img src="<?= base_url() ?>img/_sample/525x531/1.png" alt=""/>
                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <h4>1r de Primària</h4>

                    <p>Detall de tot el material per tot el curs. en cas de modificació o canvi se us informarà personalment.</p>

                    <!-- Vertical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Llegim 1</div>
                                <div>20€</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Llegim 2</div>
                                <div>20€</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Números 1</div>
                                <div>6€</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Dibuix 1</div>
                                <div>6€</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div><b>Total:</b><br/>(IVA inclòs)</div>
                                <div><b>52€</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Sign Up Today<i></i></a>

                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main ">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <h4>Nursery Fees Structure</h4>

                    <p>Morbi congue nunc sed congue vehicula phasellus erat ligula sedis fringilla sed congue id fermentum at neque nam posuere.</p>

                    <!-- Verical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>School Fees (Per Term)</div>
                                <div>$1,650.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Registration Fee (One-Time Payment)</div>
                                <div>$50.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Development Fee</div>
                                <div>$70.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Miscellaneous Fee</div>
                                <div>$180.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div><b>Total:</b><br/>(inc. VAT)</div>
                                <div><b>$1,950.00</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Sign Up Today<i></i></a>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">
                    <img src="<?= base_url() ?>img/_sample/525x531/2.png" alt=""/>
                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-background-image template-background-image-1">

        <!-- Main -->
        <div class="template-main">

            <!-- White section -->
            <div class="template-section-white">

                <!-- Header and subheader -->
                <div class="template-component-header-subheader">
                    <h2>Existing Students</h2>
                    <h6>We offer a quality children care and friendly atmosphere</h6>
                    <div></div>
                </div>

            </div>

            <!-- Pricing plans -->
            <div class="template-component-pricing-plan template-component-pricing-plan-style-1">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-component-pricing-plan-price">
                            <span>$10</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Morbi Etos Potenti
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Vestibulum convallis ipsum mi laoreet elementum ante.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                        </div>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-component-pricing-plan-price">
                            <span>$25</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Libero Augue Porta
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Praesent euismod massa quam euismod imperdiet velit.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                        </div>		
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-component-pricing-plan-price">
                            <span>$35</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Curabitur Elementum
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Praesent eros urna feugiat non maximus vitae id liberou.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Choose Plan<i></i></a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>

    </div>

</div>