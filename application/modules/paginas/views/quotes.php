<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom-background template-header-bottom-background-img-1 template-header-bottom-background-style-1">
            <div class="template-main">
                <h1>Quotes</h1>
                <h6>Una mica de números</h6>
            </div>
        </div>

    </div>
</div>
<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main ">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">
                <h2>Fem números per cicles</h2>
                <h6>Els preus són orientatius i en cada cas poden variar</h6>
                <div></div>
            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">
                    <img src="<?= base_url() ?>img/_sample/525x531/1.jpg" alt=""/>
                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <h4>Llar d'infants</h4>

                    <p>Les quotes són mensual però són orientatives. El menjador por comprar-se tiquets per dies.</p>

                    <!-- Vertical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Matrícula</div>
                                <div>60€</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Torn només matí</div>
                                <div>172€/mes</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Matí-Tarda</div>
                                <div>230€/mes</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Menjador (amb càtering)</div>
                                <div>30€/mes</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div><b>Total:</b><br/>(inc. VAT)</div>
                                <div><b>xxx</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Més informació<i></i></a>

                </div>

            </div>



        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main ">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <h4>Infantil</h4>

                    <p>Morbi congue nunc sed congue vehicula phasellus erat ligula sedis fringilla sed congue id fermentum at neque nam posuere.</p>

                    <!-- Verical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>School Fees (Per Term)</div>
                                <div>$1,650.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Registration Fee (One-Time Payment)</div>
                                <div>$50.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Development Fee</div>
                                <div>$70.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Miscellaneous Fee</div>
                                <div>$180.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div><b>Total:</b><br/>(inc. VAT)</div>
                                <div><b>$1,950.00</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Més informació<i></i></a>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">
                    <img src="<?= base_url() ?>img/_sample/525x531/2.jpg" alt=""/>
                </div>

            </div>

        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5">

        <!-- Main -->
        <div class="template-main ">

            <!-- Header and subheader -->
            <div class="template-component-header-subheader">

            </div>

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">
                    <img src="<?= base_url() ?>img/_sample/525x531/3.jpg" alt=""/>
                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">

                    <h4>Primària</h4>

                    <p>Morbi congue nunc sed congue vehicula phasellus erat ligula sedis fringilla sed congue id fermentum at neque nam posuere.</p>

                    <!-- Vertical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>School Fees (Per Term)</div>
                                <div>$1,900.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Registration Fee (One-Time Payment)</div>
                                <div>$50.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Development Fee</div>
                                <div>$100.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Miscellaneous Fee</div>
                                <div>$220.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div><b>Total:</b><br/>(inc. VAT)</div>
                                <div><b>$2,270.00</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Més informació<i></i></a>

                </div>

            </div>



        </div>

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-background-color-2">

        <!-- Main -->
        <div class="template-main ">

            <!-- Layout 50x50 -->
            <div class="template-layout-50x50 template-clear-fix">

                <!-- Left column -->
                <div class="template-layout-column-left">

                    <h4>Secundària</h4>

                    <p>Morbi congue nunc sed congue vehicula phasellus erat ligula sedis fringilla sed congue id fermentum at neque nam posuere.</p>

                    <!-- Verical grid -->
                    <div class="template-component-vertical-grid template-margin-top-3">
                        <ul>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>School Fees (Per Term)</div>
                                <div>$1,650.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Registration Fee (One-Time Payment)</div>
                                <div>$50.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div>Development Fee</div>
                                <div>$70.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-1n">
                                <div>Miscellaneous Fee</div>
                                <div>$180.00</div>
                            </li>
                            <li class="template-component-vertical-grid-line-2n">
                                <div><b>Total:</b><br/>(inc. VAT)</div>
                                <div><b>$1,950.00</b></div>
                            </li>
                        </ul>
                    </div>

                    <!-- Button -->
                    <a href="#" class="template-component-button template-component-button-style-1 template-margin-top-3">Més informació<i></i></a>

                </div>

                <!-- Right column -->
                <div class="template-layout-column-right">
                    <img src="<?= base_url() ?>img/_sample/525x531/4.jpg" alt=""/>
                </div>

            </div>

        </div>

    </div>


    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-5 template-background-image template-background-image-1">

        <!-- Main -->
        <div class="template-main">

            <!-- White section -->
            <div class="template-section-white">

                <!-- Header and subheader -->
                <div class="template-component-header-subheader">
                    <h2>Extraexcolars</h2>
                    <h6>We offer a quality children care and friendly atmosphere</h6>
                    <div></div>
                </div>

            </div>

            <!-- Pricing plans -->
            <div class="template-component-pricing-plan template-component-pricing-plan-style-1">
                <ul class="template-layout-33x33x33 template-clear-fix">
                    <li class="template-layout-column-left">
                        <div class="template-component-pricing-plan-price">
                            <span>$10</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Morbi Etos Potenti
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Vestibulum convallis ipsum mi laoreet elementum ante.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Selecciona <i></i></a>
                        </div>
                    </li>
                    <li class="template-layout-column-center">
                        <div class="template-component-pricing-plan-price">
                            <span>$25</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Libero Augue Porta
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Praesent euismod massa quam euismod imperdiet velit.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Selecciona<i></i></a>
                        </div>		
                    </li>
                    <li class="template-layout-column-right">
                        <div class="template-component-pricing-plan-price">
                            <span>$35</span>
                            <span>/ day</span>
                        </div>
                        <h5 class="template-component-pricing-plan-header">
                            Curabitur Elementum
                        </h5>
                        <div class="template-component-pricing-plan-description">
                            Praesent eros urna feugiat non maximus vitae id liberou.
                        </div>
                        <div class="template-component-pricing-plan-button">
                            <a href="#" class="template-component-button template-component-button-style-3">Selecciona<i></i></a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>

    </div>

</div>