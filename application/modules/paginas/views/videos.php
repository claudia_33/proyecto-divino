<!-- Header -->
<div class="template-header">
    <!-- Top header -->
    <?= $this->load->view('includes/template/header') ?>
    <!-- Bottom header -->
    <div class="template-header-bottom">

        <div class="template-header-bottom">

            <div class="template-header-bottom-background template-header-bottom-background-img-8 template-header-bottom-background-style-1">
                <div class="template-main">
                    <h1>Tabbed Gallery</h1>
                    <h6>Different Layouts</h6>
                </div>
            </div>

        </div>

    </div>
</div>

<!-- Content -->
<div class="template-content">

    <!-- Section -->
    <div class="template-content-section template-padding-bottom-5 template-main">

        <!-- Tabs -->
        <div class="template-component-tab">

            <!-- Navigation -->
            <ul>
                <li>
                    <a href="#template-tab-1">Valentine&#039;s Day</a>
                    <span></span>
                </li>
                <li>
                    <a href="#template-tab-2">City ZOO Trip</a>
                    <span></span>
                </li>
                <li>
                    <a href="#template-tab-3">Mother&#039; Day</a>
                    <span></span>
                </li>
            </ul>

            <!-- Tab #1 -->
            <div id="template-tab-1">

                <!-- Gallery -->
                <div class="template-component-gallery">

                    <!-- Layout 50x50 -->
                    <ul class="template-layout-50x50 template-clear-fix">

                        <!-- Left column -->
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/10.jpg" data-fancybox-group="gallery-1">
                                    <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Play Time In Kindergarten</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Play Time In Kindergarten</b> Tenderhearts Class</p>
                            </div>
                        </li>

                        <!-- Right column -->
                        <li class="template-layout-column-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/11.jpg" data-fancybox-group="gallery-1">
                                    <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Classes in Painting</h6>
                                    <span>Bouncy Bears Class</span>
                                </div>
                                <p><b>Classes in Painting</b> Bouncy Bears Class</p>
                            </div>			
                        </li>

                        <!-- Left column -->
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/4.jpg" data-fancybox-group="gallery-1">
                                    <img src="<?= base_url() ?>img/_sample/690x506/4.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Drawing and Painting Lessons</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                            </div>
                        </li>

                        <!-- Right column -->
                        <li class="template-layout-column-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/12.jpg" data-fancybox-group="gallery-1">
                                    <img src="<?= base_url() ?>img/_sample/690x506/12.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Outdoor Activity During Recess</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                            </div>			
                        </li>

                    </ul>

                </div>	

            </div>

            <!-- Tab #2 -->
            <div id="template-tab-2">

                <!-- Gallery -->
                <div class="template-component-gallery">

                    <!-- Layout 33x33x33 -->
                    <ul class="template-layout-33x33x33 template-clear-fix">

                        <!-- Left column -->
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/5.jpg" data-fancybox-group="gallery-2">
                                    <img src="<?= base_url() ?>img/_sample/690x506/5.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Classes in Writing</h6>
                                    <span>Shining Stars Class</span>
                                </div>	
                                <p><b>Classes in Writing</b> Shining Stars Class</p>
                            </div>
                        </li>

                        <!-- Center column -->
                        <li class="template-layout-column-center">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/6.jpg" data-fancybox-group="gallery-2">
                                    <img src="<?= base_url() ?>img/_sample/690x506/6.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Classes in Painting</h6>
                                    <span>Shining Stars Class</span>
                                </div>
                                <p><b>Classes in Painting</b> Shining Stars Class</p>
                            </div>			
                        </li>

                        <!-- Right column -->
                        <li class="template-layout-column-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/7.jpg" data-fancybox-group="gallery-2">
                                    <img src="<?= base_url() ?>img/_sample/690x506/7.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Drawing and Painting Lessons</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                            </div>
                        </li>

                        <!-- Left column -->
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/1.jpg" data-fancybox-group="gallery-2">
                                    <img src="<?= base_url() ?>img/_sample/690x506/1.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Outdoor Activity During Recess</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                            </div>			
                        </li>

                        <!-- Center column -->
                        <li class="template-layout-column-center">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/8.jpg" data-fancybox-group="gallery-2">
                                    <img src="<?= base_url() ?>img/_sample/690x506/8.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Columbus Day Celebration</h6>
                                    <span>Bouncy Bears Class</span>
                                </div>
                                <p><b>Columbus Day Celebration</b> Bouncy Bears Class</p>
                            </div>
                        </li>

                        <!-- Right column -->
                        <li class="template-layout-column-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/9.jpg" data-fancybox-group="gallery-2">
                                    <img src="<?= base_url() ?>img/_sample/690x506/9.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Birthday in Kindergarten</h6>
                                    <span>Little Lambs Class</span>
                                </div>
                                <p><b>Birthday in Kindergarten</b> Little Lambs Class</p>
                            </div>			
                        </li>

                    </ul>

                </div>	

            </div>

            <!-- Tab #3 -->
            <div id="template-tab-3">

                <!-- Gallery -->
                <div class="template-component-gallery">

                    <!-- Layout 25x25x25x25 -->
                    <ul class="template-layout-25x25x25x25 template-clear-fix">

                        <!-- Left column -->
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/12.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/12.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Classes in Writing</h6>
                                    <span>Shining Stars Class</span>
                                </div>	
                                <p><b>Classes in Writing</b> Shining Stars Class</p>
                            </div>
                        </li>

                        <!-- Center left column -->
                        <li class="template-layout-column-center-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/4.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/4.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Classes in Painting</h6>
                                    <span>Shining Stars Class</span>
                                </div>
                                <p><b>Classes in Painting</b> Shining Stars Class</p>
                            </div>			
                        </li>

                        <!-- Center right column -->
                        <li class="template-layout-column-center-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/11.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/11.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Classes in Painting</h6>
                                    <span>Bouncy Bears Class</span>
                                </div>
                                <p><b>Classes in Painting</b> Bouncy Bears Class</p>
                            </div>
                        </li>

                        <!-- Right column -->
                        <li class="template-layout-column-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/10.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/10.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Play Time In Kindergarten</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Play Time In Kindergarten</b> Tenderhearts Class</p>
                            </div>			
                        </li>

                        <!-- Left column -->
                        <li class="template-layout-column-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/9.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/9.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Birthday in Kindergarten</h6>
                                    <span>Little Lambs Class</span>
                                </div>
                                <p><b>Birthday in Kindergarten</b> Little Lambs Class</p>
                            </div>
                        </li>

                        <!-- Center left column -->
                        <li class="template-layout-column-center-left">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/8.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/8.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Columbus Day Celebration</h6>
                                    <span>Bouncy Bears Class</span>
                                </div>
                                <p><b>Columbus Day Celebration</b> Bouncy Bears Class</p>
                            </div>			
                        </li>

                        <!-- Center right column -->
                        <li class="template-layout-column-center-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/1.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/1.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Outdoor Activity During Recess</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Outdoor Activity During Recess</b> Tenderhearts Class</p>
                            </div>
                        </li>

                        <!-- Right column -->
                        <li class="template-layout-column-right">
                            <div class="template-component-image template-component-image-hover-slide-enable template-fancybox template-preloader">
                                <a href="<?= base_url() ?>img/_sample/1050x770/7.jpg" data-fancybox-group="gallery-3">
                                    <img src="<?= base_url() ?>img/_sample/690x506/7.jpg" alt="" />
                                    <span><span><span></span></span></span>
                                </a>
                                <div>
                                    <h6>Drawing and Painting Lessons</h6>
                                    <span>Tenderhearts Class</span>
                                </div>
                                <p><b>Drawing and Painting Lessons</b> Tenderhearts Class</p>
                            </div>			
                        </li>

                    </ul>

                </div>	

            </div>

        </div>				

    </div>

    <!-- Section -->
    <div class="template-content-section template-padding-top-reset template-padding-bottom-reset template-background-color-2">

        <!-- Main -->
        <div class="template-main">

            <!-- Call to action -->
            <div class="template-component-call-to-action template-component-call-to-action-style-2">

                <!-- Content -->
                <div class="template-component-call-to-action-content">

                    <!-- Left column -->
                    <div class="template-component-call-to-action-content-left">

                        <!-- Header -->
                        <h3>How to Enroll Your Child to a Class?</h3>

                    </div>

                    <!-- Right column -->
                    <div class="template-component-call-to-action-content-right">

                        <!-- Button -->
                        <a href="#" class="template-component-button template-component-button-style-1">Learn More<i></i></a>

                    </div>									

                </div>

            </div>				

        </div>

    </div>

</div>