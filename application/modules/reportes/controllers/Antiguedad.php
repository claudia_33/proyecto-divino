<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Antiguedad extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        /*
             *  Esto es un script que reemplaza palabras por datos. 
             *  Para el caso de las tablas repetitivas como las notas se diseña la tabla en reportes
             *  Los tr que se van a repetir en dicha tabla deben estar dentro de un tbody y los datos extras dentro de un thead o un tfooter
             *  Luego cambiar el id de la tabla en $tbody = $tbody[2]; linea 56 la cual comienza desde 0 y su maximo valor la cantidad de tablas -1
             *  El reporte debe estar diseñado para que los datos a mostrar corresponden al nombre dado en el select [plan_id] => valor_plan_id
         */
        function _reemplazar($texto){ 
            $antiguedad_materias = array();
            $qr = "
                SELECT
                `user`.cedula,
                `user`.nombre,
                `user`.apellido_paterno,
                `user`.apellido_materno,
                designacion_docentes_detalles.id as nro,
                designacion_docentes_detalles.docentes_id,
                designacion_docentes.nro_resolucion,
                designacion_docentes.facultades_id,
                designacion_docentes_detalles.obser as observacion,
                origen_resol.denominacion as origen_resol,
                categoria_docente.denominacion as categoria_docente,
                programacion_carreras.programacion_nombre,
                categ_sueldo.denominacion,
                materias_plan.id,
                materias.id as materia_id,
                materias.materia_nombre,
                cursos.curso_nombre,
                titulo_cortesia.denominacion,
                carreras.carrera_nombre,
                DATE_FORMAT(designacion_docentes_detalles.vigencia_desde,'%d-%m-%Y') as vigencia_desde,
                DATE_FORMAT(designacion_docentes_detalles.vigencia_hasta,'%d-%m-%Y') as vigencia_hasta,
                designacion_docentes_detalles.anulado,
                designacion_docentes_detalles.renuncia,
                facultades.nombre as facultad_nombre,
                DATE_FORMAT(docentes.fecha_ingreso,'%d-%m-%Y') as fecha_ingreso,
                docentes.titulo_principal
                FROM
                docentes
                INNER JOIN designacion_docentes_detalles ON designacion_docentes_detalles.docentes_id = docentes.id
                INNER JOIN `user` ON `user`.id = docentes.user_id
                INNER JOIN designacion_docentes ON designacion_docentes_detalles.designacion_docentes_id = designacion_docentes.id
                INNER JOIN categoria_docente ON categoria_docente.id = designacion_docentes_detalles.categoria_docente_id
                INNER JOIN categ_sueldo ON categ_sueldo.id = designacion_docentes_detalles.categ_sueldo_id
                INNER JOIN materias_plan ON materias_plan.id = designacion_docentes_detalles.materias_plan_id
                INNER JOIN materias ON materias_plan.materias_id = materias.id
                INNER JOIN cursos ON materias_plan.cursos_id = cursos.id
                INNER JOIN titulo_cortesia ON titulo_cortesia.id = docentes.titulo_cortesia_id
                INNER JOIN programacion_carreras ON programacion_carreras.id = designacion_docentes_detalles.programacion_carreras_id
                INNER JOIN carreras ON carreras.id = designacion_docentes_detalles.carreras_id
                INNER JOIN facultades ON facultades.id = designacion_docentes.facultades_id
                INNER JOIN origen_resol ON origen_resol.id = designacion_docentes.origen_resol_id
                WHERE designacion_docentes.facultades_id = ".$_GET['facultades_id']." and docentes.id = ".$_GET['docentes_id']."
                ORDER BY designacion_docentes_detalles.vigencia_hasta ASC
            ";
            $qr = $this->db->query($qr);
            if($qr->num_rows()>0){
                $filas = fragmentar($texto,'<tr>','</tr>',false);                
                for($i=0;$i<count($filas);$i++){
                    if($i!=6 && $i!=9 && $i!=12 && $i!=15){
                        $t = $filas[$i];
                        foreach($qr->row() as $n=>$v){
                            $t = str_replace('['.$n.']',$v,$t);
                        }
                        $texto = str_replace($filas[$i],$t,$texto);
                    }
                }
                $bucle = $filas[6];
                $bucletr = '';
                foreach($qr->result() as $ns=>$q){
                    $t = $bucle;
                    foreach($q as $n=>$v){
                        $t = str_replace('['.$n.']',$v,$t);
                    }      
                    $bucletr.= $t;
                    $antiguedad = $this->antiguedad($q);
                    $bucletr = str_replace('[antiguedad]',round($antiguedad/12,1),$bucletr);
                    //$bucletr = str_replace('[nro]',$ns+1,$bucletr);    
                    $antiguedad = round($antiguedad/12,1);
                    $antiguedad_materias[$q->materia_id] = @$antiguedad_materias[$q->materia_id]+$antiguedad;                    
                }
                
                /*foreach($antiguedad_materias as $n=>$a){
                    $as = explode(',',$a);
                    $meses = 0;
                    if(count($as)==2){
                        $as[0] = $as[0]*12;
                        $meses = $as[0]+$as[1];
                    }else{
                        $meses = $a*12;
                    }
                   // $meses = round($meses/12,2);
                    $antiguedad_materias[$n] = $meses;
                }*/
                $texto = str_replace($filas[6],$bucletr,$texto);
                $texto = str_replace('[vigencia_desde]',$qr->row()->vigencia_desde,$texto);                
                //Antiguedad por materia
                $sql = "
                    SELECT
                    MIN(designacion_docentes_detalles.vigencia_desde) as vigencia_desde,
                    MAX(designacion_docentes_detalles.vigencia_hasta) as vigencia_hasta,
                    facultades.nombre as facultad,
                    carreras.carrera_nombre as carrera,
                    materias.id as materia_id,
                    materias.materia_nombre as materia
                    FROM designacion_docentes_detalles 
                    INNER JOIN designacion_docentes ON designacion_docentes.id = designacion_docentes_detalles.designacion_docentes_id
                    INNER JOIN materias_plan ON materias_plan.id = designacion_docentes_detalles.materias_plan_id
                    INNER JOIN materias ON materias.id = materias_plan.materias_id
                    INNER JOIN programacion_carreras ON programacion_carreras.id = designacion_docentes_detalles.programacion_carreras_id
                    INNER JOIN carreras ON carreras.id = programacion_carreras.carreras_id
                    INNER JOIN facultades ON facultades.id = designacion_docentes.facultades_id
                    WHERE designacion_docentes.facultades_id = ".$_GET['facultades_id']." and docentes_id = ".$_GET['docentes_id']."
                    GROUP BY materias.id";
                
                    $qr = $this->db->query($sql);
                    $bucle = $filas[9];
                    $bucletr = '';
                    foreach($qr->result() as $q){
                        $t = $bucle;
                        foreach($q as $n=>$v){
                            $t = str_replace('['.$n.']',$v,$t);
                        }                        
                        $antiguedad = $antiguedad_materias[$q->materia_id];
                        $bucletr .= str_replace('[antiguedad]',$antiguedad,$t);                        
                    }
                    $texto = str_replace($filas[9],$bucletr,$texto);
                    
                    //Antiguedad por facultad
                    $sql = "
                    SELECT
                    designacion_docentes_detalles.vigencia_desde,
                    designacion_docentes_detalles.vigencia_hasta,
                    facultades.nombre as facultad,
                    carreras.carrera_nombre as carrera,
                    materias.materia_nombre as materia
                    FROM designacion_docentes_detalles 
                    INNER JOIN designacion_docentes ON designacion_docentes.id = designacion_docentes_detalles.designacion_docentes_id
                    INNER JOIN materias_plan ON materias_plan.id = designacion_docentes_detalles.materias_plan_id
                    INNER JOIN materias ON materias.id = materias_plan.materias_id
                    INNER JOIN programacion_carreras ON programacion_carreras.id = designacion_docentes_detalles.programacion_carreras_id
                    INNER JOIN carreras ON carreras.id = programacion_carreras.carreras_id
                    INNER JOIN facultades ON facultades.id = designacion_docentes.facultades_id
                    WHERE designacion_docentes.facultades_id = ".$_GET['facultades_id']." and docentes_id = ".$_GET['docentes_id']."
                    ";
                
                    $qr = $this->db->query($sql);
                    $bucle = $filas[12];
                    $anos = array();
                    $meses = 0;
                    
                    if($qr->num_rows()>0){                        
                        $desde = $qr->row()->vigencia_desde;
                        $hasta = $qr->row()->vigencia_hasta; 
                        $anos = $this->antiguedad($qr);                        
                    }                    
                    //print_r($anos);                    
                    $bucle = str_replace('[antiguedad]',$anos,$bucle);
                    $bucle = str_replace('[facultad]',$qr->row()->facultad,$bucle);
                    $texto = str_replace($filas[12],$bucle,$texto);
                    
                    //Antiguedad en la universidad
                    $sql = "
                    SELECT
                    vigencia_desde as vigencia_desde,
                    vigencia_hasta as vigencia_hasta
                    FROM(
                        SELECT
                        designacion_docentes_detalles.vigencia_desde,
                        designacion_docentes_detalles.vigencia_hasta,
                        facultades.nombre as facultad,
                        carreras.carrera_nombre as carrera,
                        materias.materia_nombre as materia
                        FROM designacion_docentes_detalles 
                        INNER JOIN designacion_docentes ON designacion_docentes.id = designacion_docentes_detalles.designacion_docentes_id
                        INNER JOIN materias_plan 
                        ON materias_plan.id = designacion_docentes_detalles.materias_plan_id
                        INNER JOIN materias ON materias.id = materias_plan.materias_id
                        INNER JOIN programacion_carreras ON programacion_carreras.id = designacion_docentes_detalles.programacion_carreras_id
                        INNER JOIN carreras ON carreras.id = programacion_carreras.carreras_id
                        INNER JOIN facultades ON facultades.id = designacion_docentes.facultades_id
                        WHERE docentes_id = ".$_GET['docentes_id'].") as v
                    ";
                
                    $qr = $this->db->query($sql);
                    $bucle = $filas[13];
                    $anos = array();
                    $meses = 0;
                    if($qr->num_rows()>0){
                         $desde = $qr->row()->vigencia_desde;
                         $hasta = $qr->row()->vigencia_hasta; 
                         $anos = $this->antiguedad($qr);
                    }
                    
                    //print_r($anos);                    
                    $bucle = str_replace('[antiguedad]',$anos,$bucle);
                    $texto = str_replace($filas[13],$bucle,$texto);
                return $texto;
            }            
            else{
                return 'Docente no encontrado'; 
            }
        }
        
        function draw($docentes_id = ''){
            if(!empty($_GET['docentes_id']) && !empty($_GET['facultades_id']) || (!empty($docentes_id) && !empty($this->user->facultad))){
                if(!empty($docentes_id)){
                    $_GET['docentes_id'] = $docentes_id;
                    if(!empty($this->user->facultad)){
                        $_GET['facultades_id'] = $this->user->facultad;
                    }else{
                        header("Location:".base_url('panel/seleccionarFacultad'));
                    }
                }
                $reporte = $this->db->get_where('reportes',array('identificador'=>'antiguedaddocentexfacultad'));
                if($reporte->num_rows()>0){
                    echo '<htm><head><meta charset="utf8">';
                    echo '</head><body>';
                    echo $this->_reemplazar($reporte->row()->contenido);
                    echo '</body></html>';
                }
            }else{
                $this->loadView(array('view'=>'antiguedad'));
            }
        }

        function diff($desde,$hasta){
            $datetime1 = date_create($desde);
            $datetime2 = date_create($hasta);
            $interval = date_diff($datetime1, $datetime2);            
            $mes = $interval->format("%m");
            $mes = date("d-m",strtotime($desde))=='01-01'?$mes+1:$mes;
            $meses = ($interval->format("%Y")*12)+$mes;
            $dif = $meses;            
            return $dif;
        }
        
        function antiguedad($q){
            if(!empty($q->vigencia_desde) && !empty($q->vigencia_hasta)){
                $q->vigencia_hasta = strtotime($q->vigencia_hasta)>strtotime(date("Y-m-d"))?date("Y-m-d"):$q->vigencia_hasta;
                return $this->diff($q->vigencia_desde, $q->vigencia_hasta);
            }
            return 0;
        }
    }
?>
