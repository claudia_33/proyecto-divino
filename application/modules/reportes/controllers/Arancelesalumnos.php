<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Arancelesalumnos extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        /*
             *  Esto es un script que reemplaza palabras por datos. 
             *  Para el caso de las tablas repetitivas como las notas se diseña la tabla en reportes
             *  Los tr que se van a repetir en dicha tabla deben estar dentro de un tbody y los datos extras dentro de un thead o un tfooter
             *  Luego cambiar el id de la tabla en $tbody = $tbody[2]; linea 56 la cual comienza desde 0 y su maximo valor la cantidad de tablas -1
             *  El reporte debe estar diseñado para que los datos a mostrar corresponden al nombre dado en el select [plan_id] => valor_plan_id
         */
        function _reemplazar($texto){ 
            $_POST['fecha1'] = date("Y-m-d",strtotime(str_replace('/','-',$_POST['fecha1'])));
            $_POST['fecha2'] = date("Y-m-d",strtotime(str_replace('/','-',$_POST['fecha2'])));
            $sql = "
                SELECT *, FORMAT(SUM(total_arancelsf),0,'de_DE') as total_monto, FORMAT(SUM(montosf),0,'de_DE') as total_pago, FORMAT(SUM(saldosf),0,'de_DE') as total_saldo FROM (
                SELECT                    
                    facturas.categoria_arancel_id,
                    facturas_detalles.arancel_id,                    
                    DATE_FORMAT('".$_POST['fecha1']."','%d-%m-%Y') as fecha1,
                    DATE_FORMAT('".$_POST['fecha2']."','%d-%m-%Y') as fecha2,                                        
                    `user`.apellido_paterno,
                    `user`.nombre,
                    `user`.cedula,
                    `user`.apellido_materno,                    
                    programacion_carreras.programacion_nombre,
                    derecho_arancel_detalle.total_arancel as total_arancelsf,
                    facturas_detalles.monto as montosf,
                    (derecho_arancel_detalle.total_arancel-facturas_detalles.monto) as saldosf
                    FROM
                    facturas
                    LEFT JOIN facturas_detalles ON facturas_detalles.facturas_id = facturas.id
                    LEFT JOIN derecho_arancel_detalle ON facturas_detalles.derecho_arancel_detalle_id = derecho_arancel_detalle.id
                    LEFT JOIN becas_detalle ON derecho_arancel_detalle.id = becas_detalle.derecho_arancel_detalle_id
                    LEFT JOIN becas ON becas_detalle.becas_id = becas.id                
                    INNER JOIN `user` ON facturas.user_id = `user`.id
                    LEFT JOIN derecho_arancel ON derecho_arancel.id = derecho_arancel_detalle.derecho_arancel_id
                    LEFT JOIN arancel ON derecho_arancel_detalle.arancel_id = arancel.id
                    LEFT JOIN programacion_carreras ON programacion_carreras.id = facturas.programacion_carreras_id
                    WHERE `user`.cedula = ".$_POST['cedula'].") as consulta
            ";
            $qr = $this->db->query($sql);
            $sql = "
                SELECT          
                facturas.id,
                facturas.numero_factura,
                DATE_FORMAT(facturas.fecha,'%d-%m-%Y') as fecha,
                arancel.arancel_nombre, 
                derecho_arancel_detalle.total_arancel as total_arancelsf,
                FORMAT(derecho_arancel_detalle.total_arancel,0,'de_DE') as total_arancel,
                FORMAT(facturas_detalles.monto,0,'de_DE') as monto,                    
                facturas_detalles.monto as montosf,
                FORMAT((derecho_arancel_detalle.total_arancel-facturas_detalles.monto),0,'de_DE') as saldo,
                (derecho_arancel_detalle.total_arancel-facturas_detalles.monto) as saldosf
                FROM
                facturas
                LEFT JOIN facturas_detalles ON facturas_detalles.facturas_id = facturas.id
                LEFT JOIN derecho_arancel_detalle ON facturas_detalles.derecho_arancel_detalle_id = derecho_arancel_detalle.id
                LEFT JOIN becas_detalle ON derecho_arancel_detalle.id = becas_detalle.derecho_arancel_detalle_id
                LEFT JOIN becas ON becas_detalle.becas_id = becas.id                
                INNER JOIN `user` ON facturas.user_id = `user`.id
                LEFT JOIN derecho_arancel ON derecho_arancel.id = derecho_arancel_detalle.derecho_arancel_id
                LEFT JOIN arancel ON derecho_arancel_detalle.arancel_id = arancel.id
                LEFT JOIN programacion_carreras ON programacion_carreras.id = facturas.programacion_carreras_id
                WHERE `user`.cedula = ".$_POST['cedula']."
            ";
            $cuerpo = $this->db->query($sql);
            if($qr->num_rows()>0){
                $encabezado = $qr->row();
                $tr = fragmentar($texto,'<tr','</tr>',false);
                $reach = $tr[5];
                foreach($encabezado as $n=>$v){
                    $texto = str_replace('['.$n.']',$v,$texto);
                }                
                
                $r = '';
                foreach($cuerpo->result() as $q){
                    $s = $reach;
                    foreach($q as $n=>$v){
                        $s = str_replace('['.$n.']',$v,$s);
                    }
                    $r.= $s;
                }
                $texto = str_replace($reach,$r,$texto);
                return $texto;
            }else{
                throw new Exception('Reporte no encontrado','404');
            }
        }
        
        function draw(){
            if(!empty($_POST)){
                $this->form_validation->set_rules('fecha1','Desde','required');
                $this->form_validation->set_rules('fecha2','Hasta','required');
                $this->form_validation->set_rules('cedula','Cedula','required');
                $reporte = $this->db->get_where('reportes',array('identificador'=>'historialarancelalumno','facultades_id'=>$this->user->facultad));
                if($reporte->num_rows()>0){
                    echo '<htm><head><meta charset="utf8">';
                    echo '</head><body>';
                    echo $this->_reemplazar($reporte->row()->contenido);
                    echo '</body></html>';
                }
            }else{
                $this->loadView(array('view'=>'arancelesalumnos'));
            }
        }        
    }
?>
