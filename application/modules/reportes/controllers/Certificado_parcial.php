<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Certificado_parcial extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        /*
             *  Esto es un script que reemplaza palabras por datos. 
             *  Para el caso de las tablas repetitivas como las notas se diseña la tabla en reportes
             *  Los tr que se van a repetir en dicha tabla deben estar dentro de un tbody y los datos extras dentro de un thead o un tfooter
             *  Luego cambiar el id de la tabla en $tbody = $tbody[2]; linea 56 la cual comienza desde 0 y su maximo valor la cantidad de tablas -1
             *  El reporte debe estar diseñado para que los datos a mostrar corresponden al nombre dado en el select [plan_id] => valor_plan_id
         */
        function _reemplazar($texto,$id){
            $this->load->library('enletras');
            //Certificado
            $this->db->select(
              "
                  user.nombre,
                  user.apellido_paterno,
                  user.apellido_materno,
                  user.cedula,
                  user.lugar_nacimiento,                  
                  user.fecha_nacimiento,
                  programacion_carreras.ciudad as Ciudad,
                  programacion_carreras.direccion as Direccion,
                  programacion_carreras.telefono as Telefono,
                  programacion_carreras.departamento as Departamento,
                  certificado_de_estudio.nro_folio as folio_nro,
                  certificado_de_estudio.nro_registro as registro_nro,
                  paises.pais_nombre, 
                  MIN(matriculas.anho_lectivo) as anho_lectivo, 
                  matriculas.id  as matriculasid,
                  carreras.carrera_nombre,
                  carreras.titulo_a_expedir,
                  modalidades.modalidad_nombre,
                  sedes.sede_nombre,
                  plan_estudio.id as planid,
                  certificado_de_estudio.id as certificado_id,
                  certificado_de_estudio.fecha_expedicion,
                  facultades.banner,
                  facultades.secretaria_gral,
                  facultades.decano,
                  facultades.encabezado_secretaria,
                  facultades.encabezado_decano,
                  facultades.nombre_real
            ");
            $this->db->join('estudiantes_carreras','estudiantes_carreras.id = certificado_de_estudio.estudiantes_carreras_id');
            $this->db->join('estudiantes','estudiantes.id = estudiantes_carreras.estudiantes_id');
            $this->db->join('user','user.id = estudiantes.user_id');
            $this->db->join('paises','paises.id = user.paises_id');
            //Matricula
            $this->db->join('matriculas','matriculas.estudiantes_id = estudiantes.id AND matriculas.programacion_carreras_id = estudiantes_carreras.programacion_carreras_id');
            $this->db->join('programacion_carreras','programacion_carreras.id = estudiantes_carreras.programacion_carreras_id');
            $this->db->join('carreras','carreras.id = programacion_carreras.carreras_id');
            $this->db->join('modalidades','modalidades.id = estudiantes_carreras.modalidades_id');
            $this->db->join('sedes','sedes.id = estudiantes_carreras.sedes_id');            
            //plan de estudio
            $this->db->join('plan_estudio','plan_estudio.programacion_carreras_id = estudiantes_carreras.programacion_carreras_id');
            $this->db->join('facultades','facultades.id = plan_estudio.facultades_id');
            $certificado = $this->db->get_where('certificado_de_estudio',array('certificado_de_estudio.id'=>$id));
            //Materias            
            if($certificado->num_rows()>0){
                $certificado = $certificado->row();                                
                    foreach($certificado as $n=>$v){
                        if(strstr($n,'fecha')){
                            $v = date("d/m/Y",strtotime($v));
                        }
                        $texto = str_replace('['.$n.']',$v,$texto);
                    }
                    /************************** Repetir script en caso de tablas de datos *********************************/
                    //Tener la tabla y dibujar la region donde será dibujada la nueva
                    $tbody = fragmentar($texto,'<tbody>','</tbody>');
                    $tbody = $tbody[2];
                    $tfoot = fragmentar($tbody,'<tr>','</tr>');
                    $tfoot = $tfoot[2];
                    $texto = str_replace($tbody,'[datos]',$texto);
                    
                    //Reemplazo de la palabra data
                    $trs = fragmentar($tbody,'<tr>','</tr>');
                    $strbody = '';
                    foreach($this->db->get('cursos')->result() as $c){
                        $materias = $this->db->query("
                            SELECT
                            af.anho_lectivo,
                            af.acta_doc_nro,
                            af.fecha as acta_final_fecha,
                            af.mesa_id,
                            afd.calificacion as acta_final_calificacion,
                            afd.incripcion_materias_id,
                            im.id,
                            mt.estudiantes_id,
                            mt.id,
                            mp.plan_estudio_id,
                            mp.total_horas_reloj as carga_horaria,
                            im.programacion_materias_plan_id,
                            e.user_id,
                            u.cedula,
                            u.nombre,
                            u.apellido_paterno,
                            u.apellido_materno,
                            cm.materias_plan_afines_id,
                            cm.fecha,
                            cm.calificacion,
                            cm.acta_nro,
                            m.materia_nombre,
                            mp.id,
                            cu.curso_nombre,
                            cu.id as cursos_id,
                            e.id,
                            ec.id,
                            ce.id, 
                            me.periodo_id,
                            me.denominacion,
                            pe.denominacion as periodo
                            FROM certificado_de_estudio ce
                            INNER JOIN estudiantes_carreras as ec ON ec.id = ce.estudiantes_carreras_id
                            INNER JOIN programacion_carreras as pc ON ec.programacion_carreras_id = pc.id
                            INNER JOIN acta_final as af ON af.programacion_carreras_id = pc.id
                            INNER JOIN acta_final_detalle as afd ON afd.acta_final_id = af.id
                            INNER JOIN inscripcion_materias as im ON im.id = afd.incripcion_materias_id
                            INNER JOIN programacion_materias_plan as pmp ON pmp.id = im.programacion_materias_plan_id
                            INNER JOIN materias_plan as mp ON mp.id = pmp.materias_plan_id
                            INNER JOIN materias as m ON m.id = mp.materias_id
                            LEFT JOIN estudiante_traslado et ON et.estudiantes_id = ec.estudiantes_id
                            LEFT JOIN convalidacion_materias cm ON cm.estudiante_traslado_id = et.id AND cm.materias_plan_afines_id = mp.id
                            INNER JOIN estudiantes e ON e.id = ec.estudiantes_id
                            INNER JOIN user u ON u.id = e.user_id
                            INNER JOIN matriculas mt ON mt.id = im.matriculas_id AND mt.estudiantes_id = ec.estudiantes_id
                            INNER JOIN cursos as cu ON cu.id = mp.cursos_id
                            INNER JOIN mesa as me ON me.id = af.mesa_id
                            INNER JOIN periodo as pe ON pe.id = me.periodo_id                            
                            WHERE ce.id = $id
                            AND mp.cursos_id = $c->id
                            AND afd.calificacion > 0
                            ORDER BY af.fecha, af.acta_doc_nro ASC
                        ");
                        
                        
                        if($materias->num_rows()>0){
                            $strbody.= '<tr>'.str_replace('[curso_nombre]',$c->curso_nombre,$trs[0]).'</tr>';
                            //Imprimir filas
                            //Notas
                            $promedio = 0;
                            $mat = 0;
                            $carga = 0;
                            $aprobadas = 0;
                            foreach($materias->result() as $m){
                                $tr = '<tr>'.$trs[1].'</tr>';
                                foreach($m as $n=>$v){
                                    if($n=='acta_final_calificacion'){
                                        $v = $m->calificacion==null?$v:$m->calificacion;
                                        $promedio+= $v;
                                        $mat++;
                                        if($v>1){
                                            $aprobadas++;
                                        }
                                    }
                                    
                                    if($n=='acta_doc_nro'){
                                        $v = $m->acta_nro==null?$v:$m->acta_nro;                                        
                                    }
                                    
                                    if($n=='acta_final_fecha'){
                                        $v = $m->fecha==null?$v:$m->fecha;
                                        $v = date("d-m-Y",strtotime($v));
                                    }
                                    if($n=='carga_horaria'){
                                        if($m->calificacion>1 || $m->acta_final_calificacion>1){
                                            $carga+= $v;
                                        }
                                    }
                                    if($n=='acta_final_calificacion'){
                                        $tr = str_replace('[letra]',$this->enletras->valorEnLetras($v,''),$tr);
                                    }
                                    $tr = str_replace('['.$n.']',$v,$tr);
                                    $tfoot = str_replace('['.$n.']',$v,$tfoot);
                                }                                
                                $strbody.= $tr;                                                                
                            }
                            
                            $foot = $tfoot;
                            $promedio = round($promedio/$mat,2);
                            $foot = str_replace('[promedio]',$promedio,$foot);
                            $foot = str_replace('[convertir_letra_promedio]',$this->enletras->ValorEnLetras($promedio,''),$foot);
                            $foot = str_replace('[suma_carga_horaria]',$carga,$foot);
                            $planificacion = $this->db->get_where('materias_plan',array('plan_estudio_id'=>$materias->row()->plan_estudio_id,'cursos_id'=>$materias->row()->cursos_id))->num_rows();
                            $tipo = $planificacion==$aprobadas?'Completo':'Incompleto';
                            $foot = str_replace('[evaluar_cantidad]',$tipo,$foot);
                            $strbody.= $foot;
                        }                        
                    }
                    $texto = str_replace('[datos]',$strbody,$texto);
                    $texto = str_replace('[banners]','<img alt="" src="'.base_url('img/fotos_facultades/'.$certificado->banner).'" width="615" height="93">',$texto);
                    
                    /************************** FIN Repetir script en caso de tablas de datos *********************************/
                }            
            return $texto;            
        }
        
        function draw($id){
            if(is_numeric($id)){
            $reporte = $this->db->get_where('reportes',array('identificador'=>'cparcial'));
                if($reporte->num_rows()>0){
                    echo '<htm><head><meta charset="utf8">';
                    echo '</head><body>';
                    echo $this->_reemplazar($reporte->row()->contenido,$id);
                    echo '</body></html>';
                }
            }
        }        
    }
?>
