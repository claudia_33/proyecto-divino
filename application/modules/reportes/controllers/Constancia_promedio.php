<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Constancia_promedio extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        /*
             *  Esto es un script que reemplaza palabras por datos. 
             *  Para el caso de las tablas repetitivas como las notas se diseña la tabla en reportes
             *  Los tr que se van a repetir en dicha tabla deben estar dentro de un tbody y los datos extras dentro de un thead o un tfooter
             *  Luego cambiar el id de la tabla en $tbody = $tbody[2]; linea 56 la cual comienza desde 0 y su maximo valor la cantidad de tablas -1
             *  El reporte debe estar diseñado para que los datos a mostrar corresponden al nombre dado en el select [plan_id] => valor_plan_id
         */
        function _reemplazar($texto,$id){
            $sql = "SELECT
                    constancia_de_estudio.id,
                    constancia_de_estudio.condicion_alumno,
                    acta_final.anho_lectivo,
                    `user`.cedula,
                    `user`.nombre,
                    `user`.apellido_paterno,
                    `user`.apellido_materno,
                    matriculas.anho_lectivo,
                    acta_final_detalle.calificacion,
                    cursos.pre_fijo,
                    plan_estudio.id as plan_estudio,
                    MAX(acta_final.fecha) as fecha,
                    materias_en_plan.contador as cantidad_materias_en_plan,
                    materias.materia_nombre,
                    acta_final_detalle.incripcion_materias_id,
                    carreras.carrera_nombre,
                    modalidades.modalidad_nombre,
                    facultades.nombre as facultad_nombre,
                    facultades.banner,
                    constancia_de_estudio.firma,
                    COUNT(DISTINCT inscripcion_materias.id) as cantidad_materias_aprobadas,
                    (
                        ###TRUNCATE((IF(materias_en_plan.contador%2=0,60,50)*materias_en_plan.contador)/100,0) = MINIMO MATERIA A APROBAR
                            SELECT CASE
                        WHEN COUNT(DISTINCT inscripcion_materias.id) = materias_en_plan.contador THEN 'REGULAR'
                        WHEN COUNT(DISTINCT inscripcion_materias.id) != materias_en_plan.contador AND COUNT(DISTINCT inscripcion_materias.id) > TRUNCATE((IF(materias_en_plan.contador%2=0,60,50)*materias_en_plan.contador)/100,0) THEN 'CONDICIONAL'
                        WHEN COUNT(DISTINCT inscripcion_materias.id) != materias_en_plan.contador AND COUNT(DISTINCT inscripcion_materias.id) < TRUNCATE((IF(materias_en_plan.contador%2=0,60,50)*materias_en_plan.contador)/100,0) THEN 'REGULAR'

                            END as Porcentaje

                    ) as tipo,
                    (
                        select TRUNCATE(SUM(afd.calificacion)/COUNT(afd.calificacion),2) as promedio from acta_final_detalle afd
                        INNER JOIN inscripcion_materias as im ON im.id = afd.incripcion_materias_id
                        INNER JOIN matriculas as m ON m.id = im.matriculas_id
                        WHERE m.estudiantes_id = matriculas.estudiantes_id AND afd.calificacion > 0
                    ) as promedio
                    FROM
                    constancia_de_estudio
                    LEFT JOIN estudiantes_carreras ON constancia_de_estudio.estudiantes_carreras_id = estudiantes_carreras.id
                    LEFT JOIN estudiantes ON estudiantes_carreras.estudiantes_id = estudiantes.id
                    LEFT JOIN `user` ON estudiantes.user_id = `user`.id
                    LEFT JOIN matriculas ON matriculas.estudiantes_id = estudiantes.id
                    LEFT JOIN inscripcion_materias ON inscripcion_materias.matriculas_id = matriculas.id
                    LEFT JOIN programacion_materias_plan ON inscripcion_materias.programacion_materias_plan_id = programacion_materias_plan.id
                    LEFT JOIN materias_plan ON materias_plan.id = programacion_materias_plan.materias_plan_id
                    LEFT JOIN materias on materias.id = materias_plan.materias_id
                    LEFT JOIN plan_estudio ON materias_plan.plan_estudio_id = plan_estudio.id
                    LEFT JOIN acta_final_detalle ON acta_final_detalle.incripcion_materias_id = inscripcion_materias.id
                    LEFT JOIN acta_final ON acta_final_detalle.acta_final_id = acta_final.id
                    LEFT JOIN cursos ON cursos.id = materias_plan.cursos_id
                    LEFT JOIN carreras ON carreras.id = plan_estudio.carreras_id
                    LEFT JOIN modalidades ON modalidades.id = plan_estudio.modalidades_id
                    LEFT JOIN facultades ON facultades.id = plan_estudio.facultades_id
                    LEFT JOIN(
                        SELECT incripcion_materias_id, COUNT(incripcion_materias_id) as contador, calificacion
                        FROM acta_final_detalle
                        where acta_final_detalle.calificacion > 1
                        GROUP BY incripcion_materias_id
                    ) as materias_aprobadas ON materias_aprobadas.incripcion_materias_id = inscripcion_materias.id
                    LEFT JOIN (
                        SELECT
                            cursos_id,
                            plan_estudio_id as id,
                            count(plan_estudio_id) as contador
                            FROM
                            materias_plan
                            GROUP BY plan_estudio_id, cursos_id
                    ) as materias_en_plan ON materias_en_plan.id = materias_plan.plan_estudio_id AND materias_en_plan.cursos_id = materias_plan.cursos_id
                    WHERE constancia_de_estudio.id = ".$id." AND acta_final_detalle.calificacion > 1
                    GROUP BY materias_plan.cursos_id
                    ORDER BY materias_plan.cursos_id DESC";
            $qr = $this->db->query($sql);
            if($qr->num_rows()>0){                
                foreach($qr->row() as $n=>$v){
                    $texto = str_replace('['.$n.']',$v,$texto);
                }
                
                if($qr->num_rows()>1){
                    if($qr->row()->tipo=='CONDICIONAL'){
                        $curso = $qr->row(1)->tipo.' del '.$qr->row(1)->pre_fijo.' curso y '.$qr->row(0)->tipo.' del '.$qr->row(0)->pre_fijo;
                    }else{
                        $curso = $qr->row()->tipo.' del '.$qr->row(1)->pre_fijo.' curso ';
                    }
                }else{
                    $curso = $qr->row()->tipo.' del '.$qr->row()->pre_fijo.' curso ';
                }
                
                $texto = str_replace('[curso]',$curso,$texto);
                $texto = str_replace('[fecha_actual]',strftime("%d %b %Y"),$texto);
                $texto = str_replace('[banners]','<img alt="" src="'.base_url('img/fotos_facultades/'.$qr->row()->banner).'" width="615" height="93">',$texto);
                $texto = $this->get_qr($texto,$id);
                $this->load->library('enletras');
                $promedio = $this->enletras->ValorEnLetras($qr->row()->promedio,'');
                $texto = str_replace('[promedio_letras]',$promedio,$texto);
                return $texto;
            }else{
                throw new Exception('Reporte no encontrado','404');
            }            
        }

        function encode($id){
            $encode = base64_encode($id);
            $encode = str_replace('+','_-',$encode);
            $encode = str_replace('=','..-',$encode);
            $encode = 's35a'.$encode;
            return $encode;
        }

        function get_qr($texto,$id){
            include(APPPATH.'libraries/phpqrcode/qrlib.php');                      
            $tempDir = 'img/';
            $encodeid = $this->encode($id);
            $codeContents = "
                Este documento fue generado en el sistema SIAWEB de la Universidad Nacional de Pilar con ID $id http://siaweb.unp.edu.py/administrativos/verificador/index/constancia_de_estudio/$encodeid
            ";
            $fileName = '005_file_'.md5($codeContents).'.png'; 
            $pngAbsoluteFilePath = $tempDir.$fileName;
            $urlRelativeFilePath = 'img/'.$fileName;
            if (file_exists($pngAbsoluteFilePath)) { 
                unlink($pngAbsoluteFilePath);
            }
            QRcode::png($codeContents, $pngAbsoluteFilePath);
            return str_replace('[qr]','<img src="'.base_url().$urlRelativeFilePath.'" />',$texto);

        }

        function draw($id){
            if(is_numeric($id)){
            $reporte = $this->db->get_where('reportes',array('identificador'=>'constPromedio'));
                if($reporte->num_rows()>0){
                    echo '<htm><head><meta charset="utf8">';
                    echo '</head><body>';
                    echo $this->_reemplazar($reporte->row()->contenido,$id);
                    echo '</body></html>';
                }
            }
        }        
    }
?>
