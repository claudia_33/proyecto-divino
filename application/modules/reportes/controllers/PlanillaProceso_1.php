<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class PlanillaProceso extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function get_porcentaje_asistencia($estudiantes_id,$programacion){
            $asistencia = $this->db->query("SELECT
                    TRUNCATE((COUNT(estudiantes.id)*100)/(select count(id) from asistencia_alumno where programacion_materias_plan_id = $programacion),2) as porcentaje
                    FROM asistencia_alumno aa
                    INNER JOIN asistencia_alumno_detalle as aad ON aad.asistencia_alumno_id = aa.id
                    INNER JOIN estudiantes on estudiantes.id = aad.estudiantes_id
                    INNER JOIN user on user.id = estudiantes.user_id
                    inner join programacion_materias_plan on programacion_materias_plan.id = aa.programacion_materias_plan_id
                    inner join materias_plan on materias_plan.id = programacion_materias_plan.materias_plan_id
                    inner join materias on materias_plan.materias_id = materias.id
                    WHERE aa.programacion_materias_plan_id=$programacion AND estudiantes.id = $estudiantes_id
                    GROUP BY estudiantes.id"
             );
            return $asistencia->num_rows()>0?$asistencia->row()->porcentaje:0;
        }
        
        function _reemplazar($texto,$post){
            $sql = "
                SELECT
                user.cedula,
                user.nombre,
                user.apellido_paterno,
                user.apellido_materno,
                estudiantes.id as estudiantes_id,
                (SELECT IF(parametro_evaluacion.porc_min_recup<((SUM(evaluaciones_parciales_detalles.puntos_correctos)*porc_proceso)/suma_puntos.suma_puntos_correctos),'SI','<span style=\"color:red\">NO</span>')) as derecho_recuperacion,
                (SELECT IF(parametro_evaluacion.porc_min_firma<((SUM(evaluaciones_parciales_detalles.puntos_correctos)*100)/suma_puntos.suma_puntos_correctos),'SI','<span style=\"color:red\">NO</span>')) as derecho_firma,
                (SELECT GROUP_CONCAT( epe.fecha SEPARATOR ',' ) as fecha FROM (SELECT fecha FROM evaluaciones_parciales ep WHERE programacion_materias_plan_id = ".$_POST['programacion_materias_plan_id']." AND anho_lectivo = ".$_POST['anho_lectivo']." AND tipos_evaluacion_id != 4 GROUP BY id) AS epe) as fecha,
                total_puntos.total_puntos,
                suma_puntos.suma_puntos_correctos,
                evaluaciones_parciales_detalles.porcentaje,
                tipos_evaluacion.tipos_evaluacion_nombre,
                evaluaciones_parciales.anho_lectivo,                
                evaluaciones_parciales.anulado,                
                materias.materia_nombre,
                cursos.curso_nombre,
                parametro_evaluacion.recup_reemp,
                parametro_evaluacion.recup_acum,
                parametro_evaluacion.porc_proceso,
                parametro_evaluacion.porc_final,
                parametro_evaluacion.porc_min_recup,
                parametro_evaluacion.porc_min_firma,
                parametro_evaluacion.escala_id,
                regimenes.regimen_nombre,
                evaluaciones_parciales.programacion_materias_plan_id,
                carreras.carrera_nombre,
                sedes.sede_nombre,
                facultades.nombre as facultad_nombre,
                profesores.cedula as profesor_cedula,                
                CONCAT_WS(' ',titulo_cortesia.denominacion,profesores.nombre,profesores.apellido_paterno,profesores.apellido_materno) as profesor_nombre,
                profesores.apellido_paterno as profesor_apellido,
                TRUNCATE(((SUM(evaluaciones_parciales_detalles.puntos_correctos)*porc_proceso)/suma_puntos.suma_puntos_correctos),2) as porcentajes,
                porc_proceso as suma_porcentajes,                
                GROUP_CONCAT(evaluaciones_parciales_detalles.puntos_correctos SEPARATOR ',') as puntajes,
                evaluaciones_parciales_detalles.evaluaciones_parciales_id,
                evaluaciones_parciales_detalles.inscripcion_materias_id
                FROM
                evaluaciones_parciales
                JOIN (SELECT SUM(epe.total_puntos) as suma_puntos_correctos FROM (SELECT total_puntos FROM evaluaciones_parciales ep WHERE programacion_materias_plan_id = ".$_POST['programacion_materias_plan_id']." AND anho_lectivo = ".$_POST['anho_lectivo']." AND tipos_evaluacion_id !=4 GROUP BY id) AS epe) as suma_puntos
                JOIN (SELECT SUM(epe.total_puntos) as total, GROUP_CONCAT(epe.total_puntos SEPARATOR ',' ) as total_puntos FROM (SELECT total_puntos FROM evaluaciones_parciales ep WHERE programacion_materias_plan_id = ".$_POST['programacion_materias_plan_id']." AND anho_lectivo = ".$_POST['anho_lectivo']." AND tipos_evaluacion_id != 4 GROUP BY id) AS epe) as total_puntos
                LEFT JOIN evaluaciones_parciales_detalles ON evaluaciones_parciales_detalles.evaluaciones_parciales_id = evaluaciones_parciales.id
                LEFT JOIN tipos_evaluacion ON evaluaciones_parciales.tipos_evaluacion_id = tipos_evaluacion.id
                LEFT JOIN inscripcion_materias ON inscripcion_materias.id = evaluaciones_parciales_detalles.inscripcion_materias_id
                LEFT JOIN matriculas ON inscripcion_materias.matriculas_id = matriculas.id
                LEFT JOIN estudiantes ON estudiantes.id = matriculas.estudiantes_id
                INNER JOIN `user` ON `user`.id = estudiantes.user_id
                LEFT JOIN programacion_materias_plan ON evaluaciones_parciales.programacion_materias_plan_id = programacion_materias_plan.id
                LEFT JOIN materias_plan ON programacion_materias_plan.materias_plan_id = materias_plan.id
                LEFT JOIN materias ON materias_plan.materias_id = materias.id
                LEFT JOIN cursos ON cursos.id = materias_plan.cursos_id
                LEFT JOIN parametro_evaluacion ON parametro_evaluacion.id = materias_plan.parametro_evaluacion_id
                LEFT JOIN regimenes ON regimenes.id = materias_plan.regimenes_id                
                LEFT JOIN plan_estudio ON plan_estudio.id = materias_plan.plan_estudio_id
                LEFT JOIN carreras ON carreras.id = plan_estudio.carreras_id
                LEFT JOIN sedes ON sedes.id = plan_estudio.sedes_id
                LEFT JOIN facultades ON facultades.id = plan_estudio.facultades_id
                LEFT JOIN docentes_materias ON docentes_materias.programacion_materias_plan_id = programacion_materias_plan.id
                LEFT JOIN docentes ON docentes.id = docentes_materias.docentes_id
                LEFT JOIN user as profesores on profesores.id = docentes.user_id
                LEFT JOIN titulo_cortesia on titulo_cortesia.id = docentes.titulo_cortesia_id
                WHERE evaluaciones_parciales.anho_lectivo = ".$_POST['anho_lectivo']."
                AND evaluaciones_parciales.programacion_materias_plan_id = ".$_POST['programacion_materias_plan_id']."
                AND (evaluaciones_parciales.anulado IS NULL OR evaluaciones_parciales.anulado = 0)
                AND tipos_evaluacion_id != 4
                GROUP BY inscripcion_materias.id
                ORDER BY apellido_paterno, fecha ASC 
            ";
            $qr = $this->db->query($sql);
            if($qr->num_rows()>0){
                //Tabla 1
                $tablas = fragmentar($texto,'<table','</table>',false);
                $encabezado = $qr->row();
                $enc = $tablas[0];
                foreach($encabezado as $n=>$v){
                    $enc = str_replace('['.$n.']',$v,$enc);
                }
                //Tabla 2   
                $carrete = 4; //Id de la fila repetitiva dentro del arra
                
                $filas_reporte = fragmentar($tablas[1],'<tr>','</tr>',false);
                $filareporte = $filas_reporte[$carrete];
                $filareporte = str_replace('<tr>','',$filareporte);
                $filareporte = str_replace('</tr>','',$filareporte);
                //Reemplazamos
                $filas = '';
                $nasistencia = 0;
                foreach($qr->result() as $n=>$v){
                    $escalas = $this->db->get_where('escala_detalle',array('escala_id'=>$v->escala_id));
                    $st = '<tr>'.$filareporte.'</tr>';                    
                    foreach($v as $n2=>$v2){
                        $st = str_replace('['.$n2.']',$v2,$st);
                        if($n2=='puntajes'){
                            $evq = $this->db->get_where('evaluaciones_parciales',array('tipos_evaluacion_id'=>1,'programacion_materias_plan_id'=>$_POST['programacion_materias_plan_id']));
                            $totalpuntos = 0;
                            for($evn=0;$evn<11;$evn++){
                                $valor = 0;
                                if($evq->num_rows()>$evn){
                                    $valor = $this->db->get_where('evaluaciones_parciales_detalles',array('evaluaciones_parciales_id'=>$evq->row($evn)->id,'inscripcion_materias_id'=>$v->inscripcion_materias_id));
                                    $valor = $valor->num_rows()==0?0:$valor->row()->puntos_correctos;
                                }
                                $totalpuntos+= $valor;  
                                $valor = $valor==0?'-':$valor;
                                $st = str_replace('[puntajes_'.$evn.']',$valor,$st);                                
                            }
                            
                            //Recuperatorios
                            $evq = $this->db->get_where('evaluaciones_parciales',array('tipos_evaluacion_id'=>4,'programacion_materias_plan_id'=>$_POST['programacion_materias_plan_id']));                            
                            $totalpuntosrecu = 0;     
                            $idrecup = 0;
                            $sumapuntosrecup = 0;
                            for($evn=0;$evn<3;$evn++){
                                $valor = 0;
                                if($evq->num_rows()>$evn){
                                    $this->db->select('evaluaciones_parciales_detalles.*,evaluaciones_parciales.total_puntos, estudiantes_id');
                                    $this->db->join('evaluaciones_parciales','evaluaciones_parciales_detalles.evaluaciones_parciales_id = evaluaciones_parciales.id');
                                    $this->db->join('inscripcion_materias','evaluaciones_parciales_detalles.inscripcion_materias_id = inscripcion_materias.id');
                                    $this->db->join('matriculas','inscripcion_materias.matriculas_id = matriculas.id');
                                    $qqq = $this->db->get_where('evaluaciones_parciales_detalles',array('evaluaciones_parciales_id'=>$evq->row($evn)->id,'estudiantes_id'=>$v->estudiantes_id));
                                    $valor = $qqq->num_rows()==0?0:$qqq->row()->puntos_correctos;                                    
                                    if($valor>0 && $idrecup<$qqq->row()->evaluaciones_parciales_id){
                                        $totalpuntosrecu = $valor;
                                        $idrecup = $qqq->row()->evaluaciones_parciales_id;
                                        $sumapuntosrecup = $qqq->row()->total_puntos;
                                    }
                                    
                                }                                
                                $valor = $valor<0?'Sin Cargar':$valor;
                                $valor = $valor==0?'-':$valor;
                                $st = str_replace('[recup_'.$evn.']',$valor,$st); 
                            }
                            //Si tiene recuperacion se reemplaza
                            $totalpuntos = $totalpuntosrecu > 0 ?$totalpuntosrecu:$totalpuntos;
                            $sumapuntos = $totalpuntosrecu > 0 ?$sumapuntosrecup:$v->suma_puntos_correctos;
                            $porcentajes = ($totalpuntos*$v->suma_porcentajes)/$sumapuntos;
                            $tiene_firma = ($totalpuntos*100)/$sumapuntos;
                            $tiene_firmas = $v->porc_min_firma<=$tiene_firma?'SI':'<span style="color:red">NO</span>';
                            $recuperacion = $v->porc_min_recup<=$tiene_firma?'SI':'<span style="color:red">NO</span>';
                            $calif = 0;
                            $califf = $tiene_firma;
                            //$caiff = $porcentajes+$califf;
                            foreach($escalas->result() as $e){ 
                                if($califf>=$e->desde && $califf<=$e->hasta){
                                    $calif = $e->calif;
                                } 
                            }
                            $califf = $califf==0?'-':$califf;
                            $calif = $calif==0?'-':$calif;
                            $porcentajes = $porcentajes==0?'-':$porcentajes;
                            $totalpuntos = $totalpuntos==0?'-':$totalpuntos;
                            $califf = is_numeric($califf)?number_format($califf,2,',','.'):$califf;
                            $porcentajes = is_numeric($porcentajes)?number_format($porcentajes,2,',','.'):$porcentajes;
                            $st = str_replace('[porcentajesss]',$califf,$st);
                            $st = str_replace('[calif]',$calif,$st);
                            $st = str_replace('[porcentajess]', $porcentajes,$st);
                            $st = str_replace('[total_puntos_correctos]',$totalpuntos,$st);
                            $st = str_replace('[derecho_firmas]',$tiene_firmas,$st);
                            $st = str_replace('[derecho_recuperacions]',$recuperacion,$st); 
                            $asistencia = $this->get_porcentaje_asistencia($v->estudiantes_id, $_POST['programacion_materias_plan_id']);
                            $nasistencia+= $asistencia;
                            $st = str_replace('[asistencia]',$asistencia.'%',$st);
                            
                            
                        }
                    }
                    $filas.= $st;
                }
                $filas_reporte[$carrete] = $filas;
                
                $te = '<table width="1151" cellspacing="0" cellpadding="0" border="1">';
                foreach($filas_reporte as $f){
                    $te.= $f;
                }
                $te.= '</table>';
                foreach($qr->result() as $n=>$v){                    
                    foreach($v as $n2=>$v2){
                        if($n2=='fecha' || $n2=='total_puntos'){
                            $puntajes = explode(',',$v2);
                            for($i=0;$i<11;$i++){
                                $valor = isset($puntajes[$i])?$puntajes[$i]:'';
                                $valor = !empty($valor) && $n2=='fecha'?date("d-m-Y",strtotime($valor)):$valor;
                                $te = str_replace('['.$n2.'_'.$i.']',$valor,$te);
                            }
                        }                        
                        $te = str_replace('['.$n2.']',$v2,$te);
                    }                    
                }
                $texto = str_replace($tablas[0], $enc,$texto);
                $texto = str_replace($tablas[1], $te,$texto);
                $asistencia = round($nasistencia/$qr->num_rows(),2);
                $texto = str_replace('[total_asistencia]',$asistencia.'%',$texto);
                $qr = $this->db->get_where('facultades',array('id'=>$this->user->facultad));
                $texto = str_replace('[banner]','<img src="'.base_url('img/fotos_facultades/'.$qr->row()->banner).'"  width="615" height="93">',$texto);
                
                
                ///Reempalzar fechas de recuperacion
                $recupes = $this->db->get_where('evaluaciones_parciales',array('tipos_evaluacion_id'=>4,'programacion_materias_plan_id'=>$_POST['programacion_materias_plan_id']));
                for($i=0;$i<3;$i++){
                    if($i<$recupes->num_rows()){
                        $texto = str_replace('[fecha_recu'.$i.']',date("d/m/Y",strtotime($recupes->row($i)->fecha)),$texto);
                        $texto = str_replace('[total_recup'.$i.']',$recupes->row($i)->total_puntos,$texto);
                    }else{
                        $texto = str_replace('[fecha_recu'.$i.']','',$texto);
                        $texto = str_replace('[total_recup'.$i.']','',$texto);
                    }
                }
                return $texto;
            }else{
                throw new Exception('Reporte no encontrado','404');
            }
        }
        
        function planilladeproceso($programacion_materias_plan_id = ''){
            if(is_numeric($programacion_materias_plan_id)){    
                $this->load->model('querys');
                $pro = $this->querys->get_programacion_materias_plan($programacion_materias_plan_id);
                if($pro->num_rows()>0){
                    $_POST['programacion_materias_plan_id'] = $programacion_materias_plan_id;
                    $_POST['anho_lectivo'] = $pro->row()->anho_lectivo;
                    //
                }else{
                    echo 'Reporte no encontrado';
                    die();
                }
            }
            
            if(!empty($_POST)){
                $this->form_validation->set_rules('anho_lectivo','Año Lectivo','required');
                $this->form_validation->set_rules('programacion_materias_plan_id','Programacion','required');
                if($this->form_validation->run() || !empty($programacion_materias_plan_id)){                    
                    $reporte = $this->db->get_where('reportes',array('identificador'=>'planillaproceso2'));
                    if($reporte->num_rows()>0){
                        echo '<htm><head><meta charset="utf8">';
                        echo '</head><body>';
                        echo $this->_reemplazar($reporte->row()->contenido,$_POST);
                        echo '</body></html>';
                    }
                }else{
                    if($this->user->admin==1){
                        $this->loadView(array('view'=>'PlanillaProceso','msj'=>$this->error($this->form_validation->error_string())));
                    }
                }
            }else{
                $this->loadView(array('view'=>'PlanillaProceso'));
            }
        }
    }
?>
