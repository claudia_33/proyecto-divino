<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Recaudaciondiariaxusuario extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        
        function _reemplazar($texto,$id){
            $sql = "
                    SELECT
                    facturas.id,
                    facturas.caja_diaria_id,
                    cajas.id as caja_id,
                    facturas.anulado,
                    derecho_arancel_detalle.denominacion,
                    arancel.arancel_nombre,
                    user.cedula,
                    user.nombre,
                    user.apellido_paterno,
                    user.apellido_materno,
                    cajero.nombre as cajero_nombre,
                    cajero.apellido_paterno as cajero_apellido,
                    sedes.sede_nombre,
                    facturas.id as factura_id,
                    facturas.perceptor_id,
                    facturas.anho_lectivo,
                    DATE_FORMAT(caja_diaria.fecha_apertura,'%d/%m/%Y %H:%i:%s') as fecha,
                    facturas.numero_factura,
                    facturas_detalles.cant,
                    FORMAT(facturas_detalles.monto,0,'de_DE') as monto,
                    facturas_detalles.total,
                    facturas_detalles.arancel_id,
                    FORMAT(facturas_detalles.monto*facturas_detalles.cant,0,'de_DE') as monto,
                    caja_diaria.nro_boleta,
                    bancos.denominacion as banco,
                    (
                        SELECT 
                        FORMAT(SUM(facturas_detalles.monto*facturas_detalles.cant),0,'de_DE') 
                        FROM 
                        facturas 
                        INNER JOIN facturas_detalles ON facturas_detalles.facturas_id = facturas.id
                        INNER JOIN derecho_arancel_detalle ON facturas_detalles.derecho_arancel_detalle_id = derecho_arancel_detalle.id
                        INNER JOIN arancel ON derecho_arancel_detalle.arancel_id = arancel.id
                        LEFT JOIN `user` ON `user`.id = facturas.user_id
                        LEFT JOIN cajas ON facturas.cajas_id = cajas.id
                        LEFT JOIN sedes ON cajas.sedes_id = sedes.id
                        LEFT JOIN caja_diaria ON facturas.caja_diaria_id = caja_diaria.id
                        LEFT JOIN bancos ON caja_diaria.bancos_id = bancos.id
                        LEFT JOIN perceptor ON perceptor.id = caja_diaria.perceptor_id
                        LEFT JOIN user as cajero ON cajero.id = perceptor.user_id
                        WHERE facturas.caja_diaria_id = ".$id." AND facturas.anulado=false
                    )  as total,
                    IF(facturas.anulado=1,'Anulado','') as Estado
                    FROM
                    facturas
                    INNER JOIN facturas_detalles ON facturas_detalles.facturas_id = facturas.id
                    INNER JOIN derecho_arancel_detalle ON facturas_detalles.derecho_arancel_detalle_id = derecho_arancel_detalle.id
                    INNER JOIN arancel ON derecho_arancel_detalle.arancel_id = arancel.id
                    LEFT JOIN `user` ON `user`.id = facturas.user_id
                    LEFT JOIN cajas ON facturas.cajas_id = cajas.id
                    LEFT JOIN sedes ON cajas.sedes_id = sedes.id
                    LEFT JOIN caja_diaria ON facturas.caja_diaria_id = caja_diaria.id
                    LEFT JOIN bancos ON caja_diaria.bancos_id = bancos.id
                    LEFT JOIN perceptor ON perceptor.id = caja_diaria.perceptor_id
                    LEFT JOIN user as cajero ON cajero.id = perceptor.user_id
                    WHERE facturas.caja_diaria_id = ".$id."
                    ORDER BY facturas.id ASC
            ";
            $qr = $this->db->query($sql);
            if($qr->num_rows()>0){
                $tr = fragmentar($texto,'<tr','</tr>',false);
                $reach = $tr[4];
                $encabezado = $qr->row();
                foreach($encabezado as $n=>$v){
                    $texto = str_replace('['.$n.']',$v,$texto);
                }                
                $r = '';
                foreach($qr->result() as $q){
                    $s = $reach;
                    foreach($q as $n=>$v){
                        $s = str_replace('['.$n.']',$v,$s);
                    }
                    $r.= $s;
                }
                $tr = fragmentar($texto,'<tr','</tr>',false);
                $reach = $tr[4];
                $texto = str_replace($reach,$r,$texto);                
            }
            return $texto;
        }
        
        function draw($id = ''){
            $reporte = $this->db->get_where('reportes',array('identificador'=>'recaudaciondiariaxusuario'));
            if($reporte->num_rows()>0){
                echo '<htm><head><meta charset="utf8">';
                echo '</head><body>';
                echo $this->_reemplazar($reporte->row()->contenido,$id);
                echo '</body></html>';
            }
        }
    }
?>
