<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Resumeningresomesyrubro extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        
        function _reemplazar($texto,$post){
            $sql = "
                SELECT
                    YEAR(facturas.fecha) as anho,
                    MONTH('".date("Y-m-d",strtotime(str_replace("/","-",$_POST['fecha1'])))."') as mes_desde,
                    MONTH('".date("Y-m-d",strtotime(str_replace("/","-",$_POST['fecha2'])))."') as mes_hasta,
                    facturas.id,
                    facturas.caja_diaria_id,
                    facturas.anulado,
                    arancel.arancel_nombre,
                    facturas.perceptor_id,
                    facturas.anho_lectivo,
                    facturas.fecha,
                    facturas.numero_factura,
                    facturas_detalles.cant,
                    facturas_detalles.monto,                    
                    sum(facturas_detalles.total) as total_monto,
                    facturas_detalles.arancel_id,
                    facultades.nombre,
                    facultades.nro_unidad
                    FROM
                    facturas
                    INNER JOIN facturas_detalles ON facturas_detalles.facturas_id = facturas.id
                    INNER JOIN derecho_arancel_detalle ON facturas_detalles.derecho_arancel_detalle_id = derecho_arancel_detalle.id
                    INNER JOIN arancel ON derecho_arancel_detalle.arancel_id = arancel.id
                    INNER JOIN derecho_arancel ON derecho_arancel_detalle.derecho_arancel_id = derecho_arancel.id
                    INNER JOIN facultades ON derecho_arancel.facultades_id = facultades.id
                    WHERE facturas.fecha >= '".date("Y-m-d",strtotime(str_replace("/","-",$_POST['fecha1'])))."' AND facturas.fecha <= '".date("Y-m-d",strtotime(str_replace("/","-",$_POST['fecha2'])))."' AND facturas.anulado = false
                    GROUP BY facturas_detalles.arancel_id and facultades.id
            ";
            $qr = $this->db->query($sql);
            if($qr->num_rows()>0){
                $encabezado = $qr->row();
                foreach($encabezado as $n=>$v){
                    $texto = str_replace('['.$n.']',$v,$texto);
                }
                $tr = fragmentar($texto,'<tr','</tr>',false);
                $reach = $tr[4];
                $r = '';
                foreach($qr->result() as $q){
                    $s = $reach;
                    foreach($q as $n=>$v){
                        $s = str_replace('['.$n.']',$v,$s);
                    }
                    $r.= $s;
                }
                $texto = str_replace($reach,$r,$texto);
                return $texto;
            }else{
                throw new Exception('Reporte no encontrado','404');
            }
        }
        
        function draw(){
            if(!empty($_POST)){
                $this->form_validation->set_rules('fecha1','Fecha Desde','required');
                $this->form_validation->set_rules('fecha2','Fecha Hasta','required');
                if($this->form_validation->run()){
                    $reporte = $this->db->get_where('reportes',array('identificador'=>'resumeningresoxmesyrubro'));
                    if($reporte->num_rows()>0){
                        echo '<htm><head><meta charset="utf8">';
                        echo '</head><body>';
                        echo $this->_reemplazar($reporte->row()->contenido,$_POST);
                        echo '</body></html>';
                    }
                }else{
                    $this->loadView(array('view'=>'resumeningresoxmesyrubro','msj'=>$this->error($this->form_validation->error_string())));
                }
            }else{
                $this->loadView(array('view'=>'resumeningresoxmesyrubro'));
            }
        }
    }
?>
