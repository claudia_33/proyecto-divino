<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');
            $crud->set_relation_n_n('facultades','facultad_group','facultades','grupo','facultad','{nombre}');   
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{cedula} {nombre} {apellido_paterno} {apellido_materno}');
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function log_access($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print();
            $crud->order_by('id','desc');
            $crud->display_as('log_access.cedula','Cedula')
                 ->display_as('log_access.status','Status');
            $crud->columns('user_id','fecha','nombre','ip','facultades_id','log_access.status','tipo','log_access.cedula');
            $crud->callback_column('log_access.cedula',function($val,$row){return $row->cedula;});
            $crud->callback_column('log_access.status',function($val,$row){return $row->status;});
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'M','F'=>'F'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password');            
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            }
            $crud->unset_columns('password','')
                 ->unset_delete();
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            if($crud->getParameters()=='edit' || $crud->getParameters()=='update' || $crud->getParameters()=='update_validation'){
                if(!empty($_POST) && is_numeric($y))
                {
                    $user = $this->db->get_where('user',array('id'=>$y));
                    if($user->num_rows()>0){
                        if($user->row()->cedula!=$_POST['cedula']){
                            $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
                        }
                    }
                }
            }
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->columns('foto','cedula','nombre','apellido_paterno','email','status');
            if($this->user->admin==1){
                $crud->add_action('<i class="fa fa-"></i> usar usuario','',base_url('seguridad/loginasuser').'/');
            }
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function loginasuser($id){
            if($this->user->admin==1 && is_numeric($id)){
                $userid = $this->user->id;                
                $this->user->unlog();
                $this->user->login_short($id);
                $_SESSION['admin'] = 1;
                if(!isset($_SESSION['useranterior'])){
                    $_SESSION['useranterior'] = $userid;
                }
                redirect('panel');
            }
        }
        
        function acciones($x = '',$y = ''){            
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);
            $crud->fields('cedula','nombre','apellido_paterno','apellido_materno','email','password','foto');
            $crud->field_type('password','password');
            $crud->callback_field('cedula',function($val){
                return form_input('cedula',$val,'id="field-cedula" class="form-control" readonly="true"');
            });
            $crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'300px','height'=>'300px'));
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
