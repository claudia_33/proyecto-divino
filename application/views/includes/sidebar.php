<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li>
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'administrativos'=>array('funcionarios','documentos_administrativos','resoluciones','listado_documentos'),
                        'alumnos'=>array('estudiantes','consultar_certificado','consultar_constancia','mis_carreras'),
                        'maestros'=>array(
                            'docentes',
                            'designacion_docentes',
                            'constancia_docente',
                            'docentes_materias',
                            'formacion_academica',
                            'capacitaciones_docentes',
                            'concursos/concurso_docente'
                        ),
                        'procesosacademicos'=>array('asistencia_alumno',
                            'horario_examenes',
                            'acta_final','historico_actas_finales',
                            'evaluaciones_parciales',
                            'alumnos/estudiante_traslado', 
                            'certificado_de_estudio',
                            'constancia_de_estudio',
                            'alumnos/inscripcion_mesa_final',
                            'alumnos/becas',
                            'docentes/admin_constancia_docente',
                            'materias_programadas',
                            'alumnos/eventos'),
                        'planesacademicos'=>array('programacion_carreras','estudiantes_carreras','plan_estudio','matriculas','planes_afines'),
                        'perceptoria'=>array('aranceles/arancel','aranceles/perceptor','aranceles/cajas','aranceles/condicion','aranceles/derecho_arancel'),
                        'perceptoria/facturacion'=>array('facturas','caja_diaria'),
                        'tablasMaestras'=>array(
                            'universidades',
                            'carreras',
                            'materias',
                            'facultades',
                            'paises',
                            'colegios',
                            'tiposdocumentos',
                            'institutos',
                            'modalidades',
                            'sedes',
                            'areas',
                            'regimenes',
                            'cursos',
                            'tipo_materia',
                            'formas_evaluacion',
                            'formas_recuperacion',
                            'tipos_evaluacion',
                            'parametro_evaluacion',
                            'estado_matricula',
                            'secciones',
                            'feriados',
                            'categoria_docente',
                            'asuetos',
                            'reportes'
                        ),
                        'widgets'=>array('admin/widgets'),
                        'paginas'=>array('admin/paginas'),
                        'reportes'=>array('verReportes','rep/newreportes','rep/report_organizer','rep/mis_reportes'),
                        'seguridad'=>array('grupos','funciones','user','log_access','acciones')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'certificado_de_estudio'=>array('Certificados de estudio','fa fa-file'),
                        'maestros'=>array('Docentes','fa fa-object-ungroup'),
                        'procesosacademicos'=>array('Proc. Academicos','fa fa-braille'),
                        'planesacademicos'=>array('Planes Academicos','fa fa-lightbulb-o'),
                        'perceptoria'=>array('Perceptoria','fa fa-male'),
                        'perceptoria/facturacion'=>array('Facturación','fa fa-sticky-note-o'),
                        'tablasMaestras'=>array('Tablas Maestras','fa fa-table'),
                        'admin'=>array('Admin','fa fa-lock'),
                        'reportes'=>array('Reportes','fa fa-file'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret'),
                        'admin_constancia_docente'=>array('Constancia de docentes'),
                        'docentes_materias'=>array('Mis materias'),
                        'newreportes'=>array('ReportMaker'),
                        'paginas'=>array('Paginas','fa fa-file'),
                        'log_access'=>array('Log Access'),
                        'mis_carreras'=>array('Mi Carrera')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>
        <div style="color:white; background:#222222; font-size:8px; text-align:center">
            <a href="#" style="color:white;">
                <?= img('img/eva-01.svg','width:50%') ?>
            </a>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
