(function() {
  var ImageUploader;

  ImageUploader = (function() {
    ImageUploader.imagePath = 'image.png';

    ImageUploader.imageSize = [600, 174];

    function ImageUploader(dialog) {
      this._dialog = dialog;
      this._dialog.addEventListener('cancel', (function(_this) {
        return function() {
          return _this._onCancel();
        };
      })(this));
      this._dialog.addEventListener('imageuploader.cancelupload', (function(_this) {
        return function() {
          return _this._onCancelUpload();
        };
      })(this));
      this._dialog.addEventListener('imageuploader.clear', (function(_this) {
        return function() {
          return _this._onClear();
        };
      })(this));
      this._dialog.addEventListener('imageuploader.fileready', (function(_this) {
          return function(ev) {
          
                var name, payload, regions, xhr;
                // Upload a file to the server
                var formData;
                var file = ev.detail().file;

                // Define functions to handle upload progress and completion
                xhrProgress = function (ev) {
                    // Set the progress for the upload
                    dialog.progress((ev.loaded / ev.total) * 100);
                }

                xhrComplete = function (ev) {
                    var response;

                    // Check the request is complete
                    if (ev.target.readyState != 4) {
                        return;
                    }

                    // Clear the request
                    xhr = null
                    xhrProgress = null
                    xhrComplete = null

                    // Handle the result of the upload
                    if (parseInt(ev.target.status) == 200) {
                        // Unpack the response (from JSON)
                        response = JSON.parse(ev.target.responseText);

                        // Store the image details
                        image = {
                            size: response.size,
                            url: urlImagePath+response.name
                        };

                        // Populate the dialog
                        dialog.populate(image.url, image.size);
                        ImageUploader.imagePath = urlImagePath+response.name;
                        ImageUploader.imageSize = response.size;

                    } else {
                        // The request failed, notify the user
                        new ContentTools.FlashUI('no');
                    }
                }

                // Set the dialog state to uploading and reset the progress bar to 0
                dialog.state('uploading');
                dialog.progress(0);

                // Build the form data to post to the server
                formData = new FormData();
                formData.append('image', file);

                // Make the request
                xhr = new XMLHttpRequest();
                xhr.upload.addEventListener('progress', xhrProgress);
                xhr.addEventListener('readystatechange', xhrComplete);
                xhr.open('POST', imageUploadPath, true);
                xhr.send(formData);                
            };
              
      })(this));
      this._dialog.addEventListener('imageuploader.mount', (function(_this) {
        return function() {
          return _this._onMount();
        };
      })(this));
      this._dialog.addEventListener('imageuploader.rotateccw', (function(_this) {
        return function() {
          return _this._onRotateCCW();
        };
      })(this));
      this._dialog.addEventListener('imageuploader.rotatecw', (function(_this) {
        return function() {
          return _this._onRotateCW();
        };
      })(this));
      this._dialog.addEventListener('imageuploader.save', (function(_this) {
        return function() {
          return _this._onSave();
        };
      })(this));
      this._dialog.addEventListener('imageuploader.unmount', (function(_this) {
        return function() {
          return _this._onUnmount();
        };
      })(this));
    }

    ImageUploader.prototype._onCancel = function() {};

    ImageUploader.prototype._onCancelUpload = function() {
      clearTimeout(this._uploadingTimeout);
      return this._dialog.state('empty');
    };

    ImageUploader.prototype._onClear = function() {
      return this._dialog.clear();
    };

    ImageUploader.prototype._onMount = function() {};

    ImageUploader.prototype._onRotateCCW = function() {
      var clearBusy;
      this._dialog.busy(true);
      clearBusy = (function(_this) {
        return function() {
          return _this._dialog.busy(false);
        };
      })(this);
      return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onRotateCW = function() {
      var clearBusy;
      this._dialog.busy(true);
      clearBusy = (function(_this) {
        return function() {
          return _this._dialog.busy(false);
        };
      })(this);
      return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onSave = function() {
      var clearBusy;
      this._dialog.busy(true);
      clearBusy = (function(_this) {
        return function() {
          _this._dialog.busy(false);
          return _this._dialog.save(ImageUploader.imagePath, ImageUploader.imageSize, {
            alt: 'Example of bad variable names'
          });
        };
      })(this);
      return setTimeout(clearBusy, 1500);
    };

    ImageUploader.prototype._onUnmount = function() {};

    ImageUploader.createImageUploader = function(dialog) {
      return new ImageUploader(dialog);
    };

    return ImageUploader;

  })();
  window.ImageUploader = ImageUploader;
}).call(this);
